/*
qctTypes.h
Copyright (c) 2016, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef BINLOGWINQCTTYPES_HPP
#define BINLOGWINQCTTYPES_HPP
#include <stdint.h>
#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */


typedef uint8_t QctUint8_t;     /**< An unsigned 8-bit type */
typedef int8_t QctInt8_t;       /**< A signed 8-bit type */
typedef uint16_t QctUint16_t;   /**< An unsigned 16-bit type */
typedef int16_t QctInt16_t;     /**< A signed 16-bit type */
typedef uint32_t QctUint32_t;   /**< An unsigned 32-bit type */
typedef int32_t QctInt32_t;     /**< A signed 32-bit type */
typedef uint64_t QctUint64_t;   /**< An unsigned 64-bit type */
typedef int64_t QctInt64_t;     /**< A signed 64-bit type */

/* Definitions for integer extents are provided by stdint.h. */

typedef enum QctBoolean
{
    QCT_FALSE = 0,
    QCT_TRUE = 1
} QctBoolean_t;

#ifdef __cplusplus
}
#endif
#endif
