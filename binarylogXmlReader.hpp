/*
binarylogXmlReader.hpp
Copyright (c) 2016, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef BINLOGREAD_HPP
#define BINLOGREAD_HPP
#include <iostream>
#include <map>
#include <string>
#include <vector>
#include "qctTypes.h"
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libxml/xmlmemory.h>
#include "logOsal.h"

namespace Qct
{
    enum DisplayFormat
    {
        DISPLAY_HEX,
        DISPLAY_INT,
        DISPLAY_SFN
    };

    template < typename TYPE>
    struct QctLogParam
    {
        std::string tag;
        TYPE value;
        QctUint32_t length;
        DisplayFormat dType;
        std::string displayValue;
    };

    typedef struct QctLogParam<QctUint8_t> QctUint8Param;
    typedef struct QctLogParam<QctInt8_t> QctInt8Param;
    typedef struct QctLogParam<QctUint16_t> QctUint16Param;
    typedef struct QctLogParam<QctInt16_t> QctInt16Param;
    typedef struct QctLogParam<QctUint32_t> QctUint32Param;
    typedef struct QctLogParam<QctInt32_t> QctInt32Param;
    typedef struct QctLogParam<QctUint64_t> QctUint64Param;
    typedef struct QctLogParam<QctInt64_t> QctInt64Param;
    typedef struct QctLogParam<std::string>  StringParam;
    typedef struct QctLogParam<std::string> BinaryStringParam;


    typedef std::map<std::string,std::string> DisplayData;

    typedef struct
    {
        std::vector<QctUint64Param> uint64Params;
        std::vector<QctInt64Param> int64Params;
        std::vector<QctUint32Param> uint32Params;
        std::vector<QctInt32Param> int32Params;
        std::vector<QctUint16Param> uint16Params;
        std::vector<QctInt16Param> int16Params;
        std::vector<QctUint8Param> uint8Params;
        std::vector<QctInt8Param> int8Params;
        std::vector<StringParam> strParams;
        std::vector<BinaryStringParam> binstrParams;
    }FlexiParams;

    typedef struct
    {
        FlexiParams params;
        QctUint32_t padParam;
        QctUint32_t moduleHeaderLen;
        bool mHeaderAvailable;
        std::vector<std::string> paramList;
        DisplayData modDisplayParams;
        std::string subsystemName;
        std::string moduleName;
        QctUint16_t binlogVersion;
    }ModuleDetails;

    typedef struct
    {
        FlexiParams params;
        QctUint32_t globalTick;
        QctUint16_t logId;
        std::string logName;
        std::string catName;
        std::string logString;
        std::vector<std::string> paramList;
        DisplayData logDisplayParams;
    }LogDetails;

    typedef std::map<QctUint16_t,LogDetails> LogData;

    typedef std::map<QctUint16_t,LogData> LogAllData;
    typedef std::map<QctUint16_t,ModuleDetails> ModAllData;

    //Below are xmlTags that will be looked up during parsing
    const std::string loggingSubSystemTag="LoggingSubSystem";
    const std::string logIdTag="LogId";
    const std::string moduleHeaderTag="ModuleHeader";
    const std::string subsystemTag="subsystem";
    const std::string subsystemIdTag="subsystemId";
    const std::string moduleTag="module";
    const std::string moduleidTag="moduleid";
    const std::string versionTag="binlogversion";
    const std::string logNameTag="name";
    const std::string logStringTag="logString";
    const std::string logCategoryTag="category";
    const std::string displayFormatTag="displayFormat";
    const std::string QctUint8_tag = "QctUint8_t";
    const std::string QctInt8_tag = "QctInt8_t";
    const std::string QctUint16_tag ="QctUint16_t";
    const std::string QctInt16_tag ="QctInt16_t";
    const std::string QctUint32_tag ="QctUint32_t";
    const std::string QctInt32_tag ="QctInt32_t";
    const std::string QctUint64_tag ="QctUint64_t";
    const std::string QctInt64_tag ="QctInt64_t";
    const std::string string_tag ="string";
    const std::string binarystring_tag ="binarystring";


    #define BINLOG_MAX_LOG_INT_VALUES 5
    #define BINLOG_MAX_LOG_UCHAR_VALUES 4
    #define BINLOG_MAX_LOG_STR_LEN 14
    #define BINLOG_MAX_LOG_STR_VALUES 2
    #define BINLOG_MAX_TIME_VALUE 50

    class BinaryLogXmlReader
    {

        public:
            BinaryLogXmlReader(const std::string& xmlPath);

            bool
            isGood(void) const;

            int
            getBinLogFilesFromDisk(void);

            void
            getParsedData(LogAllData &logallData,
                          ModAllData& modAllData);
        private:
            bool isGood_;
            std::string xmlPath_;
            std::map<QctUint16_t,LogData> logDetails_;
            std::map<QctUint16_t,ModuleDetails> moduleDetails_;
            std::map<QctUint16_t,std::string> logLevelStrValues_;

            int
            parseBinLogXml(
                    xmlNode * binLogRoot,
                    QctUint16_t& subModId,
                    ModuleDetails& mData,
                    LogData& lData);

            int
            parseBinLogXml(
                    xmlNode * binLogRoot);

            int
            readBinLogFileAndLoadData(void);

            int
            parseLoggingSubsystemNode(
                        xmlNodePtr& subsystemNode,
                        QctUint16_t& subModId,
                        std::string& subsystemName,
                        std::string& moduleName,
                        QctUint16_t& version);

            int
            parseCategoryNode(
                    xmlNodePtr& categoryNode,
                    const QctUint16_t& subModId);

            int
            parseModuleHeaderNode(
                    xmlNodePtr& logIdNode,
                    const QctUint16_t& subModId,
                    ModuleDetails& mData);

            int
            parseModuleHeaderParamNode(
                    xmlNodePtr& paramNode,
                    ModuleDetails& mDetails);


            int
            parseLogIdNode(
                    xmlNodePtr& logIdNode,
                    const QctUint16_t& subModId,
                    LogData& lData);

            int
            parseLogParamNode(
                    xmlNodePtr& paramNode,
                    LogDetails &lDetails);

            int tokenizeAndPushToList(
                    const std::string& nameList,
                    const std::string& typeTag,
                    const std::string& strSize,
                    const std::string& displayFormat,
                    FlexiParams &fParams,
                    std::vector<std::string>& pList);

            /// Log ids are constructed as follows:
            ///
            ///  0 1 2  3 4 5 6 7  8 9 0 1 2 3 4 5
            /// +------+----------+----------------+
            /// |      subsystem  |       module   |
            /// +-----------------+----------------+
            /// |                  id              |
            /// +------+----------+----------------+
            /// currLogId is used to maintain current log id of a module
            /// for incrementing as and when LogId tag is obtained
            QctUint16_t currLogId_;
            std::vector<std::string> binLogFileList_;
    };
}
#endif
