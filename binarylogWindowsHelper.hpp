/*
binarylogWindowsHelper.hpp
Copyright (c) 2016, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef BINLOGWINHELPER_HPP
#define BINLOGWINHELPER_HPP
#include "binarylogXmlReader.hpp"
#include <direct.h>
#include "gmodule.h"
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <epan/packet.h>
#include <epan/tvbuff-int.h>
#include <epan/filesystem.h>
#include <stdint.h>
#include "interface.h"
#include <string.h>
#include <fstream>
#include <algorithm>
#include <functional>
#include <cctype>
#include <locale>
#include <stdlib.h>

namespace Qct
{

    extern "C" int idx;
    extern "C" int numhf;

    extern "C" enum ftenum ftnone;

    extern "C" enum ftenum ftint8;
    extern "C" enum ftenum ftuint8;

    extern "C" enum ftenum ftint16;
    extern "C" enum ftenum ftuint16;

    extern "C" enum ftenum ftint24;
    extern "C" enum ftenum ftuint24;

    extern "C" enum ftenum ftint32;
    extern "C" enum ftenum ftuint32;

    extern "C" enum ftenum ftint64;
    extern "C" enum ftenum ftuint64;

    extern "C" enum ftenum ftstr;
    extern "C" enum ftenum ftbinstr;

    extern "C" enum ftenum basenone;
    extern "C" enum ftenum basehexdec;
    extern "C" enum ftenum basedec;

    extern "C" enum ftenum basenone;
    extern "C" enum ftenum basehexdec;
    extern "C" enum ftenum basedec;

    typedef struct
    {
        std::string tag;
        std::string value;
    }TagValue;

    typedef std::map<std::string,IfLogHfDataOffDetails> tagVsDetails;
    typedef struct
    {
        FlexiParams params;
        QctUint32_t padParam;
        bool mHeaderAvailable;
        QctUint32_t moduleHeaderLen; //without padding
        std::string subsystemName;
        std::string moduleName;
        QctUint16_t binlogVersion;
        //Used to calculate offset of individual params of module header from
        //starting of module header
        QctUint32_t tempOffset;
        IfModuleHeaderDetails IfDetails;
        tagVsDetails tagVsModParamDetail;
    }DissectorModuleDetails;

    typedef struct
    {
        FlexiParams params;
        QctUint32_t globalTick;
        QctUint16_t logId;
        std::string logName;
        std::string catName;
        std::string logString;
        std::vector<std::string> paramList;
        DisplayData logDisplayParams;
        //Used to calculate offset of individual params of logId from end of
        //module header
        QctUint32_t tempOffset;
        IfLogIdDetails IfDetails;
        tagVsDetails tagVsWsLogParamDetail;
    }DissectorLogDetails;


    typedef std::map<QctUint16_t,LogData> LogAlldata;
    typedef std::map<QctUint16_t,ModuleDetails> ModAlldata;


    typedef std::map<QctUint16_t,DissectorLogDetails> DissectorLogData;
    typedef std::map<QctUint16_t,DissectorLogData> DissectorLogModData;
    typedef std::map<QctUint16_t,DissectorModuleDetails> DissectorModData;

    class BinaryLogWindowsHelper
    {
        public:
            BinaryLogWindowsHelper();

            QctInt32_t
            processWsPacket(tvbuff_t *tvb,
                                packet_info *pinfo,
                                proto_tree *tree);

            QctInt32_t
            processBinLogPacket(tvbuff_t *tvb,
                                packet_info *pinfo,
                                proto_tree *tree,
                                QctUint32_t& offset,
                                proto_tree *recordSubTree,
                                QctUint32_t& binLogHdrBinLogId,
                                QctUint16_t& binLogHdrVersion);

            QctInt32_t
            processLegacyBinLogPacket(tvbuff_t *tvb,
                                packet_info *pinfo,
                                proto_tree *tree,
                                QctUint32_t& offset,
                                proto_tree *recordSubTree,
                                QctUint32_t& binLogHdrBinLogId,
                                QctUint16_t& binLogHdrVersion);

            QctInt32_t
            processSyslogPacket(tvbuff_t *tvb,
                                packet_info *pinfo,
                                proto_tree *tree,
                                QctUint32_t& offset,
                                proto_tree *recordSubTree
                                );

            void
            createLegacyLogDisplayString(std::string& colStr,
                                hf_register_info *hfRegInfo,
                                std::string& logDataDisplayStr,
                                tvbuff_t *tvb,
                                QctUint32_t offset,
                                int firstStr);
            QctInt32_t
            fillBinLogHeader(tvbuff_t *tvb,
                        packet_info *pinfo,
                        proto_tree *tree,
                        QctUint32_t& offset,
                        QctUint32_t& binLogHdrPacketType,
                        QctUint32_t& binLogHdrBinLogId,
                        QctUint16_t& binLogHdrVersion);

            void
            createLogDisplayString(std::string& logString,
                                std::vector<TagValue>& paramData,
                                std::string& logDataDisplayStr);

            void
            removeNewLineCharacters(std::string &colStr);

            void
            createProgramName(std::string& nameStr,
                            IfModuleHeaderDetails* modHeaderDetails);

            void
            getTvbValueToString(hf_register_info *hfRegInfo,
                        tvbuff_t *tvb,
                        QctUint32_t offset,
                        QctUint32_t size,
                        std::string& value);

            void
            createColumnInfoDisplayString(std::string& colStr,
                            char *globalTickString,
                            std::string& moduleHeaderParamsString,
                            std::string& logDataDisplayStr);
            int
            fillDissectorData();

            int
            fillDissectorModData(ModuleDetails& mData,
                            uint16_t modId);

            void
            fillDissectorLogData(ModuleDetails& mData,
                LogData& lData,
                QctUint16_t subsystemModId);

            void
            populateParam(enum ftenum ftType,
                    QctUint32_t paramSize,
                    std::string paramStr,
                    QctUint32_t strSize,
                    DisplayFormat displayFormat,
                    QctUint32_t& numEntries,
                    QctUint32_t& offset,
                    tagVsDetails& tagDetailMap,
                    IfLogHfDataOffDetails* dispSeq);
            int
            getLogDetailsForLogId(
                QctUint32_t binLogId,
                QctUint16_t bVersion,
                QctUint32_t packetType,
                IfLogIdDetails* LogIdDetails,
                IfModuleHeaderDetails* ModHeaderDetails,
                std::string &error);

        private:
            void
            populateHfParams(IfLogParamDetails param,
            bool fillAbbre=false);

            void
            scanParams(FlexiParams &fParam,
                QctUint32_t& numEntries,
                QctUint32_t& offset,
                tagVsDetails& tagDetailMap,
                IfLogHfDataOffDetails* dispSeq);

            LogAllData lData;
            ModAllData mData;

            DissectorModData dModData;
            DissectorLogModData dLogModData;
            std::map<QctUint16_t,std::string> logLevelStrValues_;
            std::map<int,DisplayFormat> dFormat;
            dissector_handle_t  sctpHandle;
    };
}
#endif
