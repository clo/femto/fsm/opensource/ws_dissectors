/*
interface.h
Copyright (c) 2016, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef INTERFACE_H
#define INTERFACE_H
#include "qctTypes.h"

#ifdef  __cplusplus
extern "C" {
#endif


#ifdef IS_LITTLE_ENDIAN
#undef IS_LITTLE_ENDIAN
#endif
#define IS_LITTLE_ENDIAN 1 //0
#if(IS_LITTLE_ENDIAN)
#define tvb_get_ntohs tvb_get_letohs
#define tvb_get_ntohl tvb_get_letohl
#define tvb_get_ntoh64 tvb_get_letoh64
#endif

#define MAX_HF_NUM 50000 //(LOG_MAX_ID * 9)

#define LOG_MAX_ID 2000
#define MAX_LOG_STR_VALUES 2
#define MAX_LOG_STR_LEN 14
#define MAX_LOG_INFO_STR_LEN 36
#define MAX_LOG_INT_VALUES 5
#define MAX_LOG_FLOAT_VALUES 2
#define MAX_LOG_UCHAR_VALUES 4
#define MAX_LOG_LEVEL_STR_LEN 16
#define MAX_LOG_AREA_STR_LEN 16
#define MAX_LOG_DIS_STR_LEN 128
#define MAX_LOG_DEF_STR_VALUES MAX_LOG_INT_VALUES + \
                               MAX_LOG_FLOAT_VALUES + \
                               MAX_LOG_STR_VALUES

//WS <-> XmlReader interface structures

//IfLogHfDetailsT structure holds
//hfIdx to obtain hf entry of a log and
//offset, size to fetch the corresponding value from data buffer
//and add it to the tree
typedef struct IfLogHfDataOffDetailsT {
    QctUint32_t   hfIdx;
    QctUint32_t   offset;     //offset of parameter from module header starting
    QctUint32_t   size;
    QctUint32_t   isStr;
} IfLogHfDataOffDetails;

//IfLogParamDetailsT structure holds the information needed to create hf entry
typedef struct IfLogParamDetailsT
{
    QctUint32_t hfIdx;
    char*  name;
    QctUint32_t display;
    enum ftenum fType;
} IfLogParamDetails;

typedef struct IfModuleHeaderDetailsT
{
    IfLogHfDataOffDetails moduleHeaderDispSeq[100];
    char subsystemName[50];
    char moduleName[50];
    QctUint32_t moduleHeaderLen; //without padding
    QctUint32_t logPayloadOffset;         //offset of log details from module header starting. offset = moduleHeaderLen + padParam
    QctUint32_t numModuleHeaderEntries;
} IfModuleHeaderDetails;

typedef struct IfLogIdDetailsT
{
    char categoryName[100];
    IfLogHfDataOffDetails logIdInfo;
    //same log packet can be displayed either in legacy or in binlog style
    IfLogHfDataOffDetails legacyPackDispSeq[MAX_LOG_INT_VALUES + MAX_LOG_UCHAR_VALUES + MAX_LOG_STR_VALUES];    //in case of legacy, however, only the hfidx will be used
    IfLogHfDataOffDetails binLogPackDispSeq[100];
    QctUint32_t numLegacyEntries;
    QctUint32_t numBinLogEntries;
} IfLogIdDetails;


#ifdef  __cplusplus
}
#endif

#endif  /* INTERFACE_H */
