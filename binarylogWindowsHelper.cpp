/*
binarylogWindowsHelper.cpp
Copyright (c) 2016, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "binarylogWindowsHelper.hpp"

Qct::BinaryLogXmlReader *xmlReaderPtr_ = NULL;
Qct::BinaryLogWindowsHelper *xmlBinLogWinHelperPtr_ = NULL;

//Entry point points for WS dissector
extern "C" bool WsToXmlReader(void);
extern "C" void dissect_binLog(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree);
extern "C" int readPortFromPreference(void);

extern "C" int binlogUdpPort;

extern "C" int ett_enb = -1;
extern "C" int ett_enb_lte_header = -1;

extern "C" int hf_enb_binLog_header = -1;
extern "C" int hf_enb_binLog_header_packetType = -1;
extern "C" int hf_enb_binLog_header_binLogId = -1;
extern "C" int hf_enb_binLog_header_version = -1;
extern "C" int hf_enb_binLog_header_binLogLevel = -1;
extern "C" int hf_enb_binLog_header_sequenceNumber = -1;
extern "C" int hf_enb_recordContents = -1;
extern "C" int hf_enb_recordContents_programName = -1;
extern "C" int hf_enb_recordContents_messagePayload = -1;
extern "C" int hf_enb_module_header = -1;
extern "C" int hf_enb_module_header_globalticksfn = -1;
extern "C" int hf_enb_module_header_globalticksf = -1;
extern "C" int hf_enb_lte_header = -1;
extern "C" int hf_enb_lte_header_globalticksfn = -1;
extern "C" int hf_enb_lte_header_globalticksf = -1;
extern "C" int hf_enb_lte_header_log_id = -1;
extern "C" int hf_bin_log_params = -1;
extern "C" int hf_enb_lte_binlog_category = -1;

extern "C" gint *ett[] =
{
    &ett_enb,
    &ett_enb_lte_header,
};
extern "C" hf_register_info hf[MAX_HF_NUM]=
{
    {&hf_enb_binLog_header,
    {"binLogHeader IE",
    "enb.binLog_header",
    FT_NONE,
    BASE_NONE,
    NULL,
    0x0,
    "binLogHeader IE",
    HFILL}},

    {&hf_enb_binLog_header_packetType,
    {"packetType",
    "enb.binLog_header.packetType",
    FT_UINT32,
    BASE_HEX_DEC ,
    NULL,
    0x0,
    "packetType",
    HFILL}},

    {&hf_enb_binLog_header_binLogId,
    {"binLogId",
    "enb.binLog_header.binLogId",
    FT_UINT32,
    BASE_HEX_DEC,
    NULL,
    0x0,
    "binLogId",
    HFILL}},

    {&hf_enb_binLog_header_binLogLevel,
    {"binLogLevel",
    "enb.binLog_header.binLogLevel",
    FT_STRING,
    BASE_NONE,
    NULL,
    0x0,
    "binLogLevel",
    HFILL}},

    {&hf_enb_binLog_header_version,
    {"version",
    "enb.binLog_header.version",
    FT_UINT16,
    BASE_HEX_DEC,
    NULL,
    0x0,
    "version",
    HFILL}},

    {&hf_enb_binLog_header_sequenceNumber,
    {"sequenceNumber",
    "enb.binLog_header.sequenceNumber",
    FT_UINT32,
    BASE_HEX_DEC,
    NULL,
    0x0,
    "sequenceNumber",
    HFILL}},

    {&hf_enb_recordContents,
    {"recordContents",
    "enb.recordContents",
    FT_NONE,
    BASE_NONE,
    NULL,
    0x0,
    "recordContents",
    HFILL}},

    {&hf_enb_recordContents_programName,
    {"programName",
    "enb.recordContents.programName",
    FT_STRING,
    BASE_NONE,
    NULL,
    0x0,
    "programName",
    HFILL}},

    {&hf_enb_recordContents_messagePayload,
    {"messagePayload",
    "enb.recordContents.messagePayload",
    FT_STRING,
    BASE_NONE,
    NULL,
    0x0,
    "messagePayload",
    HFILL}},

    {&hf_enb_module_header,
    {"Module Header IE",
    "enb.mod_header",
    FT_NONE,
    BASE_NONE,
    NULL,
    0x0,
    "Module Header IE",
    HFILL}},

    {&hf_enb_module_header_globalticksfn,
    {"Global Tick SFN",
    "enb.mod_header.globalTickSfn",
    FT_UINT16,
    BASE_HEX_DEC,
    NULL,
    0x0,
    "Global Tick SFN",
    HFILL}},

    {&hf_enb_module_header_globalticksf,
    {"Global Tick SF",
    "enb.mod_header.globalTickSf",
    FT_UINT16,
    BASE_HEX_DEC,
    NULL,
    0x0,
    "Global tick SF",
    HFILL}},

    {&hf_enb_lte_header,
    {"LTE Header IE",
    "enb.lte_header",
    FT_NONE,
    BASE_NONE,
    NULL,
    0x0,
    "LTE Header IE",
    HFILL}},

    {&hf_enb_lte_header_globalticksfn,
    {"Global Tick SFN",
    "enb.lte_header.globalticksfn",
    FT_UINT16,
    BASE_HEX_DEC,
    NULL,
    0x0,
    "Global Tick SFN",
    HFILL}},

    {&hf_enb_lte_header_globalticksf,
    {"Global Tick SF",
    "enb.lte_header.globalticksf",
    FT_UINT16,
    BASE_HEX_DEC,
    NULL,
    0x0,
    "Global Tick SF",
    HFILL}},

    {&hf_enb_lte_header_log_id,
    {"Log Id",
    "enb.lte_header.log_id",
    FT_UINT32,
    BASE_HEX_DEC,
    NULL,
    0x0,
    "Log Id",
    HFILL}},

    {&hf_bin_log_params,
    {"BIN_LOG_PARAMS",
    "enb.binlogParams",
    FT_NONE,
    BASE_NONE,
    NULL,
    0x0,
    "BIN_LOG_PARAMS",
    HFILL}},

    {&hf_enb_lte_binlog_category,
    {"Category",
    "enb.lte_bin_log_category",
    FT_STRING,
    BASE_NONE,
    NULL,
    0x0,
    "Category",
    HFILL}},
};

//Should match the count of the static elements
//defined in hf array defined above
extern "C" int numhf = 18;
extern "C" int idx = numhf;

#define ETH_AND_IP_OFFSET 34

extern "C"
bool WsToXmlReader(void)
{
    char *path=NULL;
    _get_pgmptr(&path);
    if(path == NULL)
    {
        g_log(NULL,G_LOG_LEVEL_CRITICAL,"WsToXmlReader _get_pgmptr failed");
        return false;
    }
    std::string pluginSuffix ="plugins\\1.8.6\\";
    std::string tmpPath=path;
    std::size_t found = tmpPath.find_last_of("/\\");
    if(found == std::string::npos)
    {
        g_log(NULL,G_LOG_LEVEL_CRITICAL,
            "WsToXmlReader failed. Unable to get the path");
        return false;
    }
    std::string pluginPath = tmpPath.substr(0,found);
    pluginPath = pluginPath + "\\" + pluginSuffix;

    xmlReaderPtr_ = new Qct::BinaryLogXmlReader(pluginPath.c_str());
    if (!xmlReaderPtr_->isGood())
    {
        g_log(NULL,G_LOG_LEVEL_CRITICAL,
            "BinaryLogXmlReader init failed. WsToXmlReader ret false ");
        return false;
    }
    xmlBinLogWinHelperPtr_ = new Qct::BinaryLogWindowsHelper();
    if(xmlBinLogWinHelperPtr_ == NULL)
    {
        g_log(NULL,G_LOG_LEVEL_CRITICAL,
            "BinaryLogWindowsHelper allocation failed. WsToXmlReader ret false ");
        return false;
    }
    if (xmlBinLogWinHelperPtr_->fillDissectorData() !=0)
    {
        g_log(NULL,G_LOG_LEVEL_CRITICAL,
            "BinaryLogWindowsHelper fillDissectorData failed."
            "WsToXmlReader ret false ");
        delete xmlBinLogWinHelperPtr_;
        xmlBinLogWinHelperPtr_ = NULL;
        return false;
    }
    return true;
}

extern "C"
void dissect_binLog (tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree)
{
    if(tree && xmlBinLogWinHelperPtr_)
    {
        xmlBinLogWinHelperPtr_->processWsPacket(tvb,pinfo,tree);
    }
    return;
}

extern "C"
int readPortFromPreference(void)
{
    char filename[]={"preferences"};
    char *path = get_persconffile_path(filename,false,false);
    if(path == NULL)
    {
        return 0;
    }
    std::string prefPath = path;
    g_free(path);
    std::ifstream preFile(prefPath);
    std::string line;

    if (preFile.is_open())
    {
        while ( getline (preFile,line) )
        {
            std::size_t tagPos = line.find("binlog.udp.port");
            if(tagPos !=std::string::npos)
            {
                std::size_t portPos = line.find(":");
                if(tagPos !=std::string::npos)
                {
                    std::string port = line.substr(portPos+1);
                    //Trim spaces if any after the : in the preferences file
                    port.erase(port.begin(),
                        std::find_if(port.begin(),
                        port.end(),
                        std::not1(std::ptr_fun<int, int>(std::isspace))));
                    port.erase(std::find_if(
                        port.rbegin(),
                        port.rend(),
                        std::not1(std::ptr_fun<int, int>(std::isspace))).base(),
                        port.end());
                    int udpPort = atoi(port.c_str());
                    preFile.close();
                    return udpPort;
                }
            }
        }
        preFile.close();
    }
    return 0;
}

namespace Qct
{
    BinaryLogWindowsHelper::BinaryLogWindowsHelper()
    {
        logLevelStrValues_[0]="";
        logLevelStrValues_[1]="CRITICAL";
        logLevelStrValues_[2]="ERROR";
        logLevelStrValues_[3]="WARN";
        logLevelStrValues_[4]="NOTICE";
        logLevelStrValues_[5]="INFO";
        logLevelStrValues_[6]="DEBUG";
        logLevelStrValues_[7]="DEVEL";
        logLevelStrValues_[8]="ALWAYS";
        sctpHandle = find_dissector("sctp");
    }
    QctInt32_t
    BinaryLogWindowsHelper::processWsPacket(tvbuff_t *tvb,
                                packet_info *pinfo,
                                proto_tree *tree)
    {
        QctInt32_t rVal = -1;
        QctUint32_t offset = 0;
        QctUint32_t binLogHdrPacketType = 0;
        QctUint32_t binLogHdrBinLogId =0;
        QctUint16_t binLogHdrVersion = 0;

        if (pinfo->destport == binlogUdpPort)
        {
            offset+=  16 ;
        }

        if(fillBinLogHeader(tvb,
                        pinfo,
                        tree,
                        offset,
                        binLogHdrPacketType,
                        binLogHdrBinLogId,
                        binLogHdrVersion)!=0)
        {
            return -1;
        }

        proto_tree *recordItem = proto_tree_add_item(tree,
                                    hf_enb_recordContents,
                                    tvb,
                                    offset,
                                    -1,
                                    ENC_NA);
        if(recordItem == NULL)
        {
            return -1;
        }
        proto_tree *recordSubtree = proto_item_add_subtree(recordItem, ett_enb);
        if(recordSubtree == NULL)
        {
            return -1;
        }
        switch(binLogHdrPacketType)
        {
            case BINLOG_PACKET_TYPE:
                processBinLogPacket(tvb,
                    pinfo,
                    tree,
                    offset,
                    recordSubtree,
                    binLogHdrBinLogId,
                    binLogHdrVersion);
            break;
            case LEGACY_PACKET_TYPE:
                processLegacyBinLogPacket(tvb,
                    pinfo,
                    tree,
                    offset,
                    recordSubtree,
                    binLogHdrBinLogId,
                    binLogHdrVersion);
            break;
            case SYSLOG_PACKET_TYPE:
                processSyslogPacket( tvb,
                    pinfo,
                    tree,
                    offset,
                    recordSubtree);
            break;
            case WIRESHARK_ETH_PACKET_TYPE:
            {
                if (sctpHandle != 0)
                {
                    tvbuff_t *sctpTvb = tvb_new_subset_remaining(tvb, offset+ETH_AND_IP_OFFSET);
                    if(sctpTvb != 0)
                    {
                        call_dissector_only(sctpHandle, sctpTvb, pinfo, tree);
                        return 0;
                    }
                }
            }
            default:
            break;
        }

        return rVal;
    }

    QctInt32_t
    BinaryLogWindowsHelper::fillBinLogHeader(tvbuff_t *tvb,
                        packet_info *pinfo,
                        proto_tree *tree,
                        QctUint32_t& offset,
                        QctUint32_t& binLogHdrPacketType,
                        QctUint32_t& binLogHdrBinLogId,
                        QctUint16_t& binLogHdrVersion)
    {
        proto_item *bHdrItem = proto_tree_add_item(tree,
                    hf_enb_binLog_header,
                    tvb,
                    offset,
                    -1,
                    ENC_NA);

        if(bHdrItem == NULL)
        {
            return -1;
        }

        proto_tree * subTreeBinLogHdr = proto_item_add_subtree(
            bHdrItem, ett_enb);

        if(subTreeBinLogHdr == NULL)
        {
            return -1;
        }
        binLogHdrPacketType = tvb_get_ntohl(tvb, offset);

        proto_tree_add_item(subTreeBinLogHdr,
            hf_enb_binLog_header_packetType,
            tvb,
            offset,
            sizeof(QctUint32_t),
            ENC_LITTLE_ENDIAN);

        offset+=sizeof(QctUint32_t);

        binLogHdrBinLogId = tvb_get_ntohl(tvb, offset);
        proto_tree_add_item(subTreeBinLogHdr,
            hf_enb_binLog_header_binLogId,
            tvb,
            offset,
            sizeof(QctUint32_t),
            ENC_LITTLE_ENDIAN);

        offset+=sizeof(QctUint32_t);

        QctUint16_t binLogHdrBinLogLevel = tvb_get_ntohs(tvb, offset);
        if(binLogHdrBinLogLevel <= QCT_LOG_LEVEL_ALWAYS)
        {
            const char *logLevel = logLevelStrValues_[binLogHdrBinLogLevel].c_str();
            proto_tree_add_string(subTreeBinLogHdr,
                hf_enb_binLog_header_binLogLevel,
                tvb,
                offset,
                sizeof(QctUint16_t),
                logLevel);
        }

        offset+=sizeof(QctUint16_t);

        binLogHdrVersion = tvb_get_ntohs(tvb, offset);
        proto_tree_add_item(subTreeBinLogHdr,
            hf_enb_binLog_header_version,
            tvb,
            offset,
            sizeof(QctUint16_t),
            ENC_LITTLE_ENDIAN);

        offset+=sizeof(QctUint16_t);

        QctUint32_t binLogHdrSeqNum = tvb_get_ntohl(tvb, offset);
        proto_tree_add_item(subTreeBinLogHdr,
            hf_enb_binLog_header_sequenceNumber,
            tvb,
            offset,
            sizeof(QctUint32_t),
            ENC_LITTLE_ENDIAN);

        offset+=sizeof(QctUint32_t);
        return 0;
    }
    QctInt32_t
    BinaryLogWindowsHelper::processBinLogPacket(tvbuff_t *tvb,
                        packet_info *pinfo,
                        proto_tree *tree,
                        QctUint32_t& offset,
                        proto_tree *recordSubTree,
                        QctUint32_t& binLogHdrBinLogId,
                        QctUint16_t& binLogHdrVersion)
    {
        IfLogIdDetails LogIdDetails;
        IfModuleHeaderDetails modHeaderDetails;
        std::string nameStr;
        std::string moduleHdrvalueStr;
        char globalTickString[100] = {'\0'};
        std::string moduleHeaderParamsString;
        std::string logDataDisplayStr;
        QctInt32_t firstStr = 0;
        QctUint32_t modHdr_globalTick = 0;
        QctUint32_t modHdr_globalTick_SFN = 0;
        QctUint32_t modHdr_globalTick_SF = 0;
        QctUint32_t modHdrOffset = 0;
        std::string colStr;
        QctUint32_t cnt = 0;
        QctUint32_t hfIdx = 0;
        QctUint32_t modHdrhfIdx = 0;
        QctUint32_t size = 0;
        std::string errorStr;

        if(getLogDetailsForLogId(binLogHdrBinLogId,
                binLogHdrVersion,
                BINLOG_PACKET_TYPE,
                &LogIdDetails,
                &modHeaderDetails,
                errorStr) )
        {
            if (check_col(pinfo->cinfo, COL_INFO))
            {
                colStr = "Binary logging dissector error: " + errorStr;
                col_add_fstr(pinfo->cinfo, COL_INFO, "%s",colStr.c_str());
                col_set_fence(pinfo->cinfo, COL_INFO);
            }
            return 1;
        }

        createProgramName(nameStr,&modHeaderDetails);

        modHdrOffset = offset;
        if(0 != modHeaderDetails.moduleHeaderLen)
        {

            proto_tree *modItem = proto_tree_add_item(recordSubTree,
                                    hf_enb_module_header,
                                    tvb,
                                    offset,
                                    -1,
                                    ENC_NA);
            if(modItem == NULL)
            {
                return -1;
            }

            proto_tree *subtree_moduleHeaderIE  = proto_item_add_subtree(
                modItem,
                ett_enb);

            if(subtree_moduleHeaderIE == NULL)
            {
                return -1;
            }

            if(0 != modHeaderDetails.numModuleHeaderEntries)
            {
                moduleHeaderParamsString += "[";
                for(cnt = 0;
                    cnt < modHeaderDetails.numModuleHeaderEntries;
                    cnt++)
                {
                    moduleHdrvalueStr="";
                    hfIdx = modHeaderDetails.moduleHeaderDispSeq[cnt].hfIdx;
                    size = modHeaderDetails.moduleHeaderDispSeq[cnt].size;
                    if(0 == modHeaderDetails.moduleHeaderDispSeq[cnt].isStr
                        &&  0 != hfIdx)
                    {
                        //For special display formats like SF-SFN which is
                        //present in certain module header,
                        //pick from the displayFormat maps based on the hfIdx as
                        //the hf table cannot provide this information.
                        DisplayFormat dispFormat = DISPLAY_INT;
                        std::map<int,DisplayFormat>::iterator dItr = dFormat.find(hfIdx);
                        if(dItr != dFormat.end())
                        {
                            dispFormat = dItr->second;
                        }
                        if(dispFormat == DISPLAY_SFN)
                        {
                            modHdr_globalTick       = tvb_get_ntohl(tvb, offset);
                            modHdr_globalTick_SFN   = (modHdr_globalTick/10)%1024;
                            modHdr_globalTick_SF    = modHdr_globalTick%10;

                            _snprintf_s(globalTickString,
                                sizeof(globalTickString),
                                sizeof(globalTickString),
                                "[ %lu - %lu]",
                                modHdr_globalTick_SFN,modHdr_globalTick_SF);

                            proto_tree_add_uint(subtree_moduleHeaderIE,
                                                hf_enb_module_header_globalticksfn ,
                                                tvb,
                                                offset, size, modHdr_globalTick_SFN);
                            proto_tree_add_uint(subtree_moduleHeaderIE,
                                                hf_enb_module_header_globalticksf ,
                                                tvb,
                                                offset, size, modHdr_globalTick_SF);
                        }
                        else
                        {
                            getTvbValueToString(&hf[hfIdx],
                                tvb,
                                offset,
                                size,
                                moduleHdrvalueStr);
                            moduleHeaderParamsString = moduleHeaderParamsString +
                                hf[hfIdx].hfinfo.name +
                                "=" +
                                moduleHdrvalueStr + " ";

                            proto_tree_add_item(subtree_moduleHeaderIE,
                                *(hf[hfIdx].p_id), tvb, offset, size,
                                ENC_LITTLE_ENDIAN);
                        }
                    }
                    else if(1 == modHeaderDetails.moduleHeaderDispSeq[cnt].isStr
                        && 0 != hfIdx)
                    {
                        proto_tree_add_item(subtree_moduleHeaderIE,
                        *(hf[hfIdx].p_id), tvb, offset, size, ENC_ASCII);

                        getTvbValueToString(&hf[hfIdx],
                            tvb,
                            offset,
                            size,
                            moduleHdrvalueStr);

                        moduleHeaderParamsString =
                            moduleHeaderParamsString +
                            hf[hfIdx].hfinfo.name +
                            "=" +
                            moduleHdrvalueStr +
                            " ";
                    }
                    offset += size;
                }
                moduleHeaderParamsString += "]";
            }
            offset += (modHeaderDetails.logPayloadOffset
                        - modHeaderDetails.moduleHeaderLen);
        }

        hfIdx = LogIdDetails.logIdInfo.hfIdx;
        modHdrhfIdx = hfIdx;

        if (check_col(pinfo->cinfo, COL_PROTOCOL))
        {
            col_set_str(pinfo->cinfo, COL_PROTOCOL, "BINLOG");
            col_set_fence(pinfo->cinfo, COL_PROTOCOL);
        }
        colStr=hf[hfIdx].hfinfo.name;
        std::string logString = hf[hfIdx].hfinfo.name;
        if(LogIdDetails.numBinLogEntries != 0)
        {
            proto_tree *logItem = proto_tree_add_item(recordSubTree,
                        hf_bin_log_params,
                        tvb,
                        offset,
                        -1,
                        ENC_NA);

            if(logItem == NULL)
            {
                return -1;
            }

            proto_tree *logSubTree = proto_item_add_subtree(
                logItem,
                ett_enb_lte_header);

            if(logSubTree == NULL)
            {
                return -1;
            }

            std::vector<TagValue> paramData;
            for(cnt = 0; cnt < LogIdDetails.numBinLogEntries; cnt++)
            {
                std::string logValueStr;
                hfIdx = LogIdDetails.binLogPackDispSeq[cnt].hfIdx;
                size = LogIdDetails.binLogPackDispSeq[cnt].size;
                if(0 == LogIdDetails.binLogPackDispSeq[cnt].isStr && 0 != hfIdx)
                {
                    proto_tree_add_item(logSubTree, *(hf[hfIdx].p_id),
                    tvb, modHdrOffset + LogIdDetails.binLogPackDispSeq[cnt].offset,
                    size, ENC_LITTLE_ENDIAN);

                }
                else if(1 == LogIdDetails.binLogPackDispSeq[cnt].isStr && 0 != hfIdx)
                {
                    proto_tree_add_item(logSubTree, *(hf[hfIdx].p_id),
                    tvb, modHdrOffset + LogIdDetails.binLogPackDispSeq[cnt].offset,
                    size, ENC_ASCII);
                }

                getTvbValueToString(&hf[hfIdx],
                    tvb,
                    modHdrOffset + LogIdDetails.binLogPackDispSeq[cnt].offset,
                    size,
                    logValueStr);

                TagValue tmp;
                tmp.tag = hf[hfIdx].hfinfo.name;
                tmp.value = logValueStr;
                paramData.push_back(tmp);
            }
            createLogDisplayString(logString,paramData,logDataDisplayStr);
        }
        else
        {
            logDataDisplayStr = logString;
        }


        if (check_col(pinfo->cinfo, COL_INFO))
        {
            createColumnInfoDisplayString(colStr,
                globalTickString,
                moduleHeaderParamsString,
                logDataDisplayStr);
            col_add_fstr(pinfo->cinfo, COL_INFO, "%s",colStr.c_str());
            col_set_fence(pinfo->cinfo, COL_INFO);
        }
        proto_tree_add_string(tree,
            hf_enb_recordContents_programName,
            tvb,
            0,
            0,
            nameStr.c_str());

        proto_tree_add_string(tree,
            hf_enb_lte_binlog_category,
            tvb,
            0,
            0,
            LogIdDetails.categoryName);
        return 0;
    }

    QctInt32_t
    BinaryLogWindowsHelper::processLegacyBinLogPacket(tvbuff_t *tvb,
                        packet_info *pinfo,
                        proto_tree *tree,
                        QctUint32_t& offset,
                        proto_tree *recordSubTree,
                        QctUint32_t& binLogHdrBinLogId,
                        QctUint16_t& binLogHdrVersion)
    {
        QctInt32_t offset_counter = 0;
        IfLogIdDetails LogIdDetails;
        IfModuleHeaderDetails modHeaderDetails;
        QctUint32_t lteHdr_globalTick = 0;
        QctUint32_t lteHdr_globalTick_SFN = 0;
        QctUint32_t lteHdr_globalTick_SF = 0;
        QctInt32_t offsetInt = 0;
        QctInt32_t offsetChar = 0;
        QctInt32_t offsetStr = 0;
        std::string colStr;
        std::string logDataDisplayStr;
        QctUint32_t cnt = 0;
        QctUint32_t hfIdx = 0;
        char globalTickString[100] = {'\0'};
        std::string nameStr;
        std::string moduleHeaderParamsString;
        QctInt32_t firstStr = 0;
        QctUint32_t size = 0;
        std::string errorStr;

        if(getLogDetailsForLogId(binLogHdrBinLogId,
                binLogHdrVersion,
                LEGACY_PACKET_TYPE,
                &LogIdDetails,
                &modHeaderDetails,
                errorStr) )
        {
            if (check_col(pinfo->cinfo, COL_INFO))
            {
                colStr = "Binary logging dissector error: " + errorStr;
                col_add_fstr(pinfo->cinfo, COL_INFO, "%s",colStr.c_str());
                col_set_fence(pinfo->cinfo, COL_INFO);
            }
            return 1;
        }

        createProgramName(nameStr,&modHeaderDetails);

        proto_tree *legacyTree = proto_tree_add_item(recordSubTree,
            hf_enb_lte_header,
            tvb,
            offset,
            -1,
            ENC_NA);

        if(legacyTree == NULL)
        {
            return -1;
        }


        proto_tree *legacySubTree = proto_item_add_subtree(legacyTree, ett_enb);
        if(legacySubTree == NULL)
        {
            return -1;
        }

        lteHdr_globalTick = tvb_get_ntohl(tvb, offset);
        lteHdr_globalTick_SFN = (lteHdr_globalTick/10)%1024;
        lteHdr_globalTick_SF = lteHdr_globalTick%10;
        proto_tree_add_uint(legacySubTree, hf_enb_lte_header_globalticksfn,
            tvb, offset, sizeof(QctUint32_t), lteHdr_globalTick_SFN);
        proto_tree_add_uint(legacySubTree, hf_enb_lte_header_globalticksf,
            tvb, offset, sizeof(QctUint32_t),lteHdr_globalTick_SF);

        _snprintf_s(globalTickString,
            sizeof(globalTickString),
            sizeof(globalTickString),"[ %lu - %lu]",
            lteHdr_globalTick_SFN,lteHdr_globalTick_SF);


        offset+=sizeof(QctUint32_t);

        proto_tree *logItem = proto_tree_add_item(recordSubTree, hf_bin_log_params,
                    tvb, offset , -1, ENC_NA);
        if(logItem == NULL)
        {
            return -1;
        }
        proto_tree *legacyLog = proto_item_add_subtree(logItem, ett_enb_lte_header);
        if(legacyLog == NULL)
        {
            return -1;
        }

        hfIdx = LogIdDetails.logIdInfo.hfIdx;

        if (check_col(pinfo->cinfo, COL_PROTOCOL))
        {
            col_set_str(pinfo->cinfo, COL_PROTOCOL, "BIN-LEGACY");
            col_set_fence(pinfo->cinfo, COL_PROTOCOL);
        }

        colStr = hf[LogIdDetails.logIdInfo.hfIdx].hfinfo.name;
        logDataDisplayStr += colStr;
        logDataDisplayStr += " ";


        offsetInt = 0;
        offsetChar = MAX_LOG_INT_VALUES * sizeof(int32_t);
        offsetStr = MAX_LOG_INT_VALUES * sizeof(int32_t) +
            MAX_LOG_UCHAR_VALUES * sizeof(uint8_t);

        for(cnt = 0;
            cnt < (MAX_LOG_INT_VALUES + MAX_LOG_UCHAR_VALUES + MAX_LOG_STR_VALUES);
            cnt++)
        {
            hfIdx = LogIdDetails.legacyPackDispSeq[cnt].hfIdx;
            size = LogIdDetails.legacyPackDispSeq[cnt].size;
            if(0 == LogIdDetails.legacyPackDispSeq[cnt].isStr &&
                sizeof(QctUint32_t) == size && 0 != hfIdx)
            {
                proto_tree_add_item(legacyLog, *(hf[hfIdx].p_id),
                    tvb, offset + offsetInt, sizeof(int32_t), ENC_LITTLE_ENDIAN);
                createLegacyLogDisplayString(colStr,
                    &hf[hfIdx],
                    logDataDisplayStr,
                    tvb,
                    offset + offsetInt,
                    0);

                offsetInt += sizeof(QctUint32_t);

            }
            else if(0 == LogIdDetails.legacyPackDispSeq[cnt].isStr &&
                sizeof(uint8_t) == size && 0 != hfIdx)
            {
                proto_tree_add_item(legacyLog, *(hf[hfIdx].p_id),
                    tvb, offset + offsetChar, sizeof(int8_t), ENC_LITTLE_ENDIAN);
                createLegacyLogDisplayString(colStr,
                    &hf[hfIdx],
                    logDataDisplayStr,
                    tvb,
                    offset + offsetChar,
                    0);
                offsetChar += sizeof(uint8_t);

            }
            else if(1 == LogIdDetails.legacyPackDispSeq[cnt].isStr && 0 != hfIdx)
            {
                proto_tree_add_item(legacyLog, *(hf[hfIdx].p_id),
                    tvb, offset + offsetStr, MAX_LOG_STR_LEN, ENC_ASCII);
                if(firstStr == 0)
                {
                    createLegacyLogDisplayString(colStr,
                        &hf[hfIdx],
                        logDataDisplayStr,
                        tvb,
                        offset + offsetStr,
                        1);
                    firstStr = 1;
                }
                else
                {
                    createLegacyLogDisplayString(colStr,
                        &hf[hfIdx],
                        logDataDisplayStr,
                        tvb,
                        offset + offsetStr,
                        0);
                }
                offsetStr += MAX_LOG_STR_LEN;
            }
        }
        if (check_col(pinfo->cinfo, COL_INFO))
        {
            createColumnInfoDisplayString(colStr,
                globalTickString,
                moduleHeaderParamsString,
                logDataDisplayStr);
            col_add_fstr(pinfo->cinfo,
                COL_INFO, "%s",
                colStr.c_str());
            col_set_fence(pinfo->cinfo, COL_INFO);
        }
        proto_tree_add_string(tree,
            hf_enb_recordContents_programName,
            tvb,
            0,
            0,
            nameStr.c_str());
        proto_tree_add_string(tree,
            hf_enb_lte_binlog_category,
            tvb,
            0,
            0,
            LogIdDetails.categoryName);
        offset += MAX_LOG_INT_VALUES * sizeof(int32_t) +
            MAX_LOG_UCHAR_VALUES * sizeof(uint8_t) +
            MAX_LOG_STR_VALUES * MAX_LOG_STR_LEN;
        return 0;
    }

    QctInt32_t
    BinaryLogWindowsHelper::processSyslogPacket(tvbuff_t *tvb,
                        packet_info *pinfo,
                        proto_tree *tree,
                        QctUint32_t& offset,
                        proto_tree *recordSubTree)
    {
        std::string colStr;
        std::string nameStr;
        char* progNameBuff=NULL;
        char* mBuff=NULL;
        if (check_col(pinfo->cinfo, COL_PROTOCOL))
        {
            col_set_str(pinfo->cinfo, COL_PROTOCOL, "BIN-SYSLOG");
            col_set_fence(pinfo->cinfo, COL_PROTOCOL);
        }
        //Program name
        progNameBuff = (char*)tvb_get_string(tvb,offset,16);
        if(progNameBuff)
        {
            nameStr = progNameBuff;
            g_free(progNameBuff);
        }

        proto_tree_add_item(recordSubTree,
                            hf_enb_recordContents_programName, tvb, offset,
                            sizeof(char) * 16,
                            ENC_ASCII);

        offset+=sizeof(char) * 16;

        mBuff = (char*)tvb_get_string(tvb,offset,tvb->length-offset);

        if(mBuff)
        {
            colStr = mBuff;

            if (check_col(pinfo->cinfo, COL_INFO))
            {
                removeNewLineCharacters(colStr);
                col_add_fstr(pinfo->cinfo, COL_INFO, "%s", colStr.c_str());
                col_set_fence(pinfo->cinfo, COL_INFO);
            }

            proto_tree_add_item(recordSubTree,
                                hf_enb_recordContents_messagePayload,
                                tvb,
                                offset,
                                strlen(mBuff),
                                ENC_ASCII);

            g_free(mBuff);
        }
        return 0;
    }
    void
    BinaryLogWindowsHelper::scanParams(FlexiParams &fParam,
        QctUint32_t& numEntries,
        QctUint32_t& offset,
        tagVsDetails& tagDetailMap,
        IfLogHfDataOffDetails* dispSeq)
    {

        if(fParam.uint64Params.size()>0)
        {
            std::vector<QctUint64Param>::iterator itr= fParam.uint64Params.begin();
            for(;itr!=fParam.uint64Params.end();itr++)
            {
                    QctUint64Param &curParam = *itr;
                    populateParam(ftuint64,
                        sizeof(uint64_t),
                        curParam.tag,
                        0,
                        curParam.dType,
                        numEntries,
                        offset,
                        tagDetailMap,
                        dispSeq);
            }
        }
        if(fParam.int64Params.size()>0)
        {
            std::vector<QctInt64Param>::iterator itr= fParam.int64Params.begin();
            for(;itr!=fParam.int64Params.end();itr++)
            {
                    QctInt64Param &curParam = *itr;
                    populateParam(ftint64,
                        sizeof(int64_t),
                        curParam.tag,
                        0,
                        curParam.dType,
                        numEntries,
                        offset,
                        tagDetailMap,
                        dispSeq);
            }
        }
        if(fParam.uint32Params.size()>0)
        {
            std::vector<QctUint32Param>::iterator itr= fParam.uint32Params.begin();
            for(;itr!=fParam.uint32Params.end();itr++)
            {
                    QctUint32Param &curParam = *itr;
                    populateParam(ftuint32,
                        sizeof(QctUint32_t),
                        curParam.tag,
                        0,
                        curParam.dType,
                        numEntries,
                        offset,
                        tagDetailMap,
                        dispSeq);
            }
        }
        if(fParam.int32Params.size()>0)
        {
            std::vector<QctInt32Param>::iterator itr= fParam.int32Params.begin();
            for(;itr!=fParam.int32Params.end();itr++)
            {
                    QctInt32Param &curParam = *itr;
                    populateParam(ftint32,
                        sizeof(int32_t),
                        curParam.tag,
                        0,
                        curParam.dType,
                        numEntries,
                        offset,
                        tagDetailMap,
                        dispSeq);
            }
        }
        if(fParam.uint16Params.size()>0)
        {
            std::vector<QctUint16Param>::iterator itr= fParam.uint16Params.begin();
            for(;itr!=fParam.uint16Params.end();itr++)
            {
                    QctUint16Param &curParam = *itr;
                    populateParam(ftuint16,
                        sizeof(QctUint16_t),
                        curParam.tag,
                        0,
                        curParam.dType,
                        numEntries,
                        offset,
                        tagDetailMap,
                        dispSeq);
            }
        }
        if(fParam.int16Params.size()>0)
        {
            std::vector<QctInt16Param>::iterator itr= fParam.int16Params.begin();
            for(;itr!=fParam.int16Params.end();itr++)
            {
                    QctInt16Param &curParam = *itr;
                    populateParam(ftint16,
                        sizeof(int16_t),
                        curParam.tag,
                        0,
                        curParam.dType,
                        numEntries,
                        offset,
                        tagDetailMap,
                        dispSeq);
            }
        }
        if(fParam.uint8Params.size()>0)
        {
            std::vector<QctUint8Param>::iterator itr= fParam.uint8Params.begin();
            for(;itr!=fParam.uint8Params.end();itr++)
            {
                    QctUint8Param &curParam = *itr;
                    populateParam(ftuint8,
                        sizeof(uint8_t),
                        curParam.tag,
                        0,
                        curParam.dType,
                        numEntries,
                        offset,
                        tagDetailMap,
                        dispSeq);
            }
        }
        if(fParam.int8Params.size()>0)
        {
            std::vector<QctInt8Param>::iterator itr= fParam.int8Params.begin();
            for(;itr!=fParam.int8Params.end();itr++)
            {
                    QctInt8Param &curParam = *itr;
                    populateParam(ftint8,
                        sizeof(int8_t),
                        curParam.tag,
                        0,
                        curParam.dType,
                        numEntries,
                        offset,
                        tagDetailMap,
                        dispSeq);
            }
        }
        if(fParam.strParams.size()>0)
        {
            std::vector<StringParam>::iterator itr= fParam.strParams.begin();
            for(;itr!=fParam.strParams.end();itr++)
            {
                    StringParam &curParam = *itr;
                    populateParam(ftstr,
                        sizeof(uint8_t),
                        curParam.tag,
                        curParam.length,
                        curParam.dType,
                        numEntries,
                        offset,
                        tagDetailMap,
                        dispSeq);
            }
        }
        if(fParam.binstrParams.size()>0)
        {
            std::vector<BinaryStringParam>::iterator itr= fParam.binstrParams.begin();
            for(;itr!=fParam.binstrParams.end();itr++)
            {
                    BinaryStringParam &curParam = *itr;
                    populateParam(ftbinstr,
                        sizeof(uint8_t),
                        curParam.tag,
                        curParam.length,
                        curParam.dType,
                        numEntries,
                        offset,
                        tagDetailMap,
                        dispSeq);
            }
        }
    }
    void BinaryLogWindowsHelper::populateParam(enum ftenum ftType,
        QctUint32_t paramSize,
        std::string paramStr,
        QctUint32_t strSize,
        DisplayFormat displayFormat,
        QctUint32_t& numEntries,
        QctUint32_t& offset,
        tagVsDetails& tagDetailMap,
        IfLogHfDataOffDetails* dispSeq)
    {

            IfLogParamDetails param;
            param.hfIdx   = numhf;
            param.name    = (char*) paramStr.c_str();

            if(displayFormat == DISPLAY_HEX)
            {
                param.display = basehexdec;
            }
            else
            {
                param.display = basedec;
            }
            dFormat[param.hfIdx] = displayFormat;
            dispSeq[numEntries].hfIdx  = numhf;
            dispSeq[numEntries].offset = offset;
            dispSeq[numEntries].isStr  = 0;

            IfLogHfDataOffDetails ws;
            ws.hfIdx      = numhf;
            ws.offset     = offset;
            ws.isStr      = 0;

            param.fType   = ftType;
            populateHfParams(param);
            dispSeq[numEntries].size   = paramSize;
            ws.size                    = paramSize;
            if(ftType == ftstr || ftType == ftbinstr)
            {
                dispSeq[numEntries].isStr = 1;
                ws.isStr      = 1;
                param.fType   = ftType;
                param.display = basenone;
                populateHfParams(param);
                if(strSize != 0)
                {
                    dispSeq[numEntries].size = strSize * sizeof(uint8_t);
                }
                else
                {
                    dispSeq[numEntries].size = BINLOG_MAX_LOG_STR_LEN * sizeof(uint8_t);
                }
                ws.size   = dispSeq[numEntries].size;
            }
            tagDetailMap[param.name] = ws;
            offset += dispSeq[numEntries].size;
            numhf+=1;
            numEntries += 1;
    }
    int
    BinaryLogWindowsHelper::fillDissectorModData(
    ModuleDetails& mData,
    QctUint16_t subsystemModId)
    {
        DissectorModData::iterator dItr = dModData.find(subsystemModId);
        if(dItr == dModData.end())
        {
            DissectorModuleDetails dmData;
            memset(&(dmData.IfDetails), 0x0, sizeof(IfModuleHeaderDetails));

            dmData.binlogVersion = mData.binlogVersion;
            dmData.moduleName = mData.moduleName;
            dmData.subsystemName = mData.subsystemName;
            dmData.moduleHeaderLen = mData.moduleHeaderLen;
            dmData.mHeaderAvailable = mData.mHeaderAvailable;

            _snprintf_s(dmData.IfDetails.moduleName,
                    sizeof(dmData.IfDetails.moduleName),
                    sizeof(dmData.IfDetails.moduleName),
                    "%s",
                    mData.moduleName.c_str());

            _snprintf_s(dmData.IfDetails.subsystemName,
                    sizeof(dmData.IfDetails.subsystemName),
                    sizeof(dmData.IfDetails.subsystemName),
                    "%s",
                    mData.subsystemName.c_str());

            dmData.IfDetails.moduleHeaderLen = mData.moduleHeaderLen;
            dmData.IfDetails.logPayloadOffset =  mData.moduleHeaderLen +
                                                 mData.padParam;


            FlexiParams &fParam = mData.params;

            scanParams(fParam,
                dmData.IfDetails.numModuleHeaderEntries,
                dmData.tempOffset,
                dmData.tagVsModParamDetail,
                &(dmData.IfDetails.moduleHeaderDispSeq[0]));

            dModData[subsystemModId]= dmData;
        }
        else
        {
            return -1;
        }
        return 0;
    }

    void
    BinaryLogWindowsHelper::fillDissectorLogData(
    ModuleDetails& mData,
    LogData& modLogData,
    QctUint16_t subsystemModId)
    {
        QctUint32_t offset=0;
        if(true == mData.mHeaderAvailable)
        {
            offset = mData.moduleHeaderLen + mData.padParam;
        }
        DissectorLogData dLogData;
        LogData::iterator modLogDataItr = modLogData.begin();
        for(;modLogDataItr!=modLogData.end();modLogDataItr++)
        {
            QctUint16_t curLogId = modLogDataItr->first;
            LogDetails& lDetails = modLogDataItr->second;
            DissectorLogDetails curLogDetails;
            memset(&(curLogDetails.IfDetails), 0x0, sizeof(IfLogIdDetails));
            curLogDetails.tempOffset = offset;

            //Push log string first
            IfLogParamDetails param;
            param.hfIdx = numhf;
            param.name = (char *)(lDetails.logString.c_str());
            param.display = basenone;
            param.fType = ftnone;
            populateHfParams(param,true);
            curLogDetails.IfDetails.logIdInfo.hfIdx = param.hfIdx;
            numhf+= 1;

            curLogDetails.IfDetails.logIdInfo.offset = curLogDetails.tempOffset;
            curLogDetails.IfDetails.logIdInfo.size = sizeof(QctUint32_t);

            curLogDetails.globalTick = lDetails.globalTick;
            curLogDetails.logId = lDetails.logId;
            curLogDetails.logName = lDetails.logName;
            curLogDetails.catName = lDetails.catName;
            curLogDetails.logString = lDetails.logString;
            curLogDetails.tempOffset = offset;

            _snprintf_s(curLogDetails.IfDetails.categoryName,
                sizeof(curLogDetails.IfDetails.categoryName),
                sizeof(curLogDetails.IfDetails.categoryName),
                "%s",curLogDetails.catName.c_str());

            FlexiParams &fParam = lDetails.params;

            scanParams(fParam,
                curLogDetails.IfDetails.numLegacyEntries,
                curLogDetails.tempOffset,
                curLogDetails.tagVsWsLogParamDetail,
                &(curLogDetails.IfDetails.legacyPackDispSeq[0]));

            size_t startParam = curLogDetails.logString.find("{");
            size_t endParam = curLogDetails.logString.find("}");
            if(startParam == std::string::npos || endParam == std::string::npos)
            {
                //For log without display format, display sequentially what is
                //specified in the xml
                for(unsigned int i=0;i<curLogDetails.IfDetails.numLegacyEntries;i++)
                {
                    curLogDetails.IfDetails.binLogPackDispSeq[i] =
                        curLogDetails.IfDetails.legacyPackDispSeq[i];
                }
                curLogDetails.IfDetails.numBinLogEntries =
                    curLogDetails.IfDetails.numLegacyEntries;
            }
            else
            {
                do
                {
                    std::string searchParam =
                        curLogDetails.
                        logString.
                        substr(startParam+1,endParam-startParam-1);

                    tagVsDetails::const_iterator tItr =
                        curLogDetails.tagVsWsLogParamDetail.find(searchParam);
                    if(tItr != curLogDetails.tagVsWsLogParamDetail.end())
                    {
                        curLogDetails.
                            IfDetails.
                            binLogPackDispSeq[curLogDetails.IfDetails.numBinLogEntries].
                            hfIdx = tItr->second.hfIdx;
                        curLogDetails.
                            IfDetails.
                            binLogPackDispSeq[curLogDetails.IfDetails.numBinLogEntries].
                            offset = tItr->second.offset;
                        curLogDetails.
                            IfDetails.
                            binLogPackDispSeq[curLogDetails.IfDetails.numBinLogEntries].
                            size = tItr->second.size;
                        curLogDetails.
                            IfDetails.
                            binLogPackDispSeq[curLogDetails.IfDetails.numBinLogEntries].
                            isStr = tItr->second.isStr;
                        curLogDetails.
                            IfDetails.numBinLogEntries += 1;
                    }
                    startParam = curLogDetails.logString.find("{", endParam + 1);
                    endParam   = curLogDetails.logString.find("}", endParam + 1);
                    if(startParam == std::string::npos || endParam == std::string::npos)
                    {
                        break;
                    }
                }while(1);
            }
            dLogData[curLogId]=curLogDetails;
        }
        dLogModData[subsystemModId]=dLogData;
    }

    int
    BinaryLogWindowsHelper::fillDissectorData()
    {
        LogAllData tmplogAllData;
        ModAllData tmpModAllData;
        xmlReaderPtr_->getParsedData(tmplogAllData,tmpModAllData);

        ModAllData::iterator mItr=tmpModAllData.begin();

        for(;mItr!=tmpModAllData.end();mItr++)
        {
            QctUint16_t subsystemModId = mItr->first;
            ModuleDetails &mData = mItr->second;
            if(fillDissectorModData(mData,subsystemModId) !=0)
            {
                return -1;
            }
            std::map<QctUint16_t,LogData>::iterator lModIdMapItr =
                tmplogAllData.find(subsystemModId);
            if(lModIdMapItr != tmplogAllData.end())
            {
                LogData &lData = lModIdMapItr->second;
                fillDissectorLogData(mData,lData,subsystemModId);
            }
            else
            {
                return -1;
            }
        }
        return 0;
    }

    int
    BinaryLogWindowsHelper::getLogDetailsForLogId(
        QctUint32_t binLogId,
        QctUint16_t bVersion,
        QctUint32_t packetType,
        IfLogIdDetails* LogIdDetails,
        IfModuleHeaderDetails* modHeaderDetails,
        std::string& errorStr)
    {
        QctUint32_t logId = binLogId;
        QctUint16_t  subsystemModId = logId >> 16;
        QctUint16_t lId = logId & 0xFFFF;

        std::map<QctUint16_t,DissectorModuleDetails>::iterator mModIdMapItr  =
            dModData.find(subsystemModId);
        DissectorLogModData::iterator lModIdMapItr  =
            dLogModData.find(subsystemModId);

        if(mModIdMapItr != dModData.end())
        {
            DissectorModuleDetails mDetails = mModIdMapItr->second;
            if(bVersion != mDetails.binlogVersion)
            {
                errorStr = "Mismatch in the log version and xml version";
                return -1;
            }
            if(mDetails.mHeaderAvailable == true)
            {
                *modHeaderDetails = mDetails.IfDetails;
            }
        }

        if(lModIdMapItr != dLogModData.end())
        {
            DissectorLogData& logDataRef = lModIdMapItr->second;
            DissectorLogData::iterator logDataItr = logDataRef.find(lId);
            DissectorLogDetails lDetails = logDataItr->second;
            *LogIdDetails = lDetails.IfDetails;
        }
        else
        {
            errorStr = "logId not found in xmls available";
            return -1;
        }
        return 0;
    }

    void
    BinaryLogWindowsHelper::populateHfParams(IfLogParamDetails param,bool fillAbbre)
    {
        _snprintf_s((char *)(hf[param.hfIdx].hfinfo.name),
                MAX_LOG_DIS_STR_LEN,
                MAX_LOG_DIS_STR_LEN,
                "%s",
                param.name);
        //The hf info abbrev should contain proper variable names. The logString used for
        //display may contain space/other chars,
        //so use logString as the abbrev name for those fields
        if(fillAbbre == false)
        {
            _snprintf_s((char *)(hf[param.hfIdx].hfinfo.abbrev),
                    MAX_LOG_DIS_STR_LEN,
                    MAX_LOG_DIS_STR_LEN,
                    "%s",
                    param.name);
        }
        else
        {
            _snprintf_s((char *)(hf[param.hfIdx].hfinfo.abbrev),
                    MAX_LOG_DIS_STR_LEN,
                    MAX_LOG_DIS_STR_LEN,
                    "logString");
        }
        hf[param.hfIdx].hfinfo.type = param.fType;
        hf[param.hfIdx].hfinfo.display = (enum field_display_e) param.display;
        _snprintf_s((char *)(hf[param.hfIdx].hfinfo.blurb),
                MAX_LOG_DIS_STR_LEN,
                MAX_LOG_DIS_STR_LEN,
                "%s",
                param.name);
        hf[param.hfIdx].hfinfo.strings = NULL;
        hf[param.hfIdx].hfinfo.bitmask = 0x0;
        hf[param.hfIdx].hfinfo.id = -1;
        hf[param.hfIdx].hfinfo.parent = 0;
        hf[param.hfIdx].hfinfo.ref_type = HF_REF_TYPE_NONE;
        hf[param.hfIdx].hfinfo.bitshift = 0;
        hf[param.hfIdx].hfinfo.same_name_next = NULL;
        hf[param.hfIdx].hfinfo.same_name_prev = NULL;
    }

    void
    BinaryLogWindowsHelper::createLogDisplayString(
    std::string& logString,
    std::vector<TagValue>& paramData,
    std::string& logDataDisplayStr)
    {
        logDataDisplayStr = logString;
        size_t startParam = logDataDisplayStr.find("{");
        size_t endParam = logDataDisplayStr.find("}");
        if(startParam == std::string::npos || endParam == std::string::npos)
        {
            logDataDisplayStr = logDataDisplayStr + " ";
            //For log without display format, display sequentially what is
            //specified in the xml
            std::vector<TagValue>::const_iterator lItr = paramData.begin();
            for(;lItr!=paramData.end();lItr++)
            {
                const TagValue& tVal = *lItr;
                std::string pValue = tVal.tag + "=" + tVal.value;
                logString = logString + pValue + " ";
            }
        }
        else
        {
            do
            {
                std::string searchParam =
                    logDataDisplayStr.substr(startParam+1,endParam-startParam-1);
                std::vector<TagValue>::const_iterator lItr = paramData.begin();
                for(;lItr!=paramData.end();lItr++)
                {
                    const TagValue& tVal = *lItr;
                    if(tVal.tag == searchParam)
                    {
                        std::string displayValue = tVal.tag + "=" + tVal.value;
                        logDataDisplayStr.replace(startParam,
                                endParam-startParam+1,
                                displayValue);
                        break;
                    }
                }
                startParam = logDataDisplayStr.find("{");
                endParam   = logDataDisplayStr.find("}");
                if(startParam == std::string::npos || endParam == std::string::npos)
                {
                        break;
                }
            }while(1);
        }
        logDataDisplayStr = "[" + logDataDisplayStr + "]";
    }

    void
    BinaryLogWindowsHelper::removeNewLineCharacters(
    std::string& colStr)
    {
        size_t pos = 0;
        std::string searchStr="\n";
        while ((pos = colStr.find(searchStr, pos)) != std::string::npos)
        {
             colStr.replace(pos, searchStr.length(), "");
        }
    }

    void
    BinaryLogWindowsHelper::createProgramName(
    std::string& nameStr,
    IfModuleHeaderDetails* modHeaderDetails)
    {
        nameStr = nameStr +
                "[ " +
                modHeaderDetails->subsystemName +
                ":" +
                modHeaderDetails->moduleName + " ]";
    }

    void
    BinaryLogWindowsHelper::getTvbValueToString(
    hf_register_info *hfRegInfo,
    tvbuff_t *tvb,
    QctUint32_t offset,
    QctUint32_t size,
    std::string& value)
    {
        guint64 dataVal;
        char tmpStr[500]={0};
        char *strBuff;
        switch (hfRegInfo->hfinfo.type)
        {
            case FT_UINT8:
                dataVal = tvb_get_guint8(tvb,offset);
                if(BASE_HEX_DEC == hfRegInfo->hfinfo.display)
                {
                    _snprintf_s(tmpStr,sizeof(tmpStr),sizeof(tmpStr), "%x", dataVal);
                }
                else
                {
                    _snprintf_s(tmpStr,sizeof(tmpStr),sizeof(tmpStr), "%u", dataVal);
                }
                break;
            case FT_UINT16:
                dataVal = tvb_get_ntohs(tvb,offset);
                if(BASE_HEX_DEC == hfRegInfo->hfinfo.display)
                {
                    _snprintf_s(tmpStr,sizeof(tmpStr),sizeof(tmpStr), "%x", dataVal);
                }
                else
                {
                    _snprintf_s(tmpStr,sizeof(tmpStr),sizeof(tmpStr), "%u", dataVal);
                }
                break;
            case FT_UINT32:
                dataVal = tvb_get_ntohl(tvb,offset);
                if(BASE_HEX_DEC == hfRegInfo->hfinfo.display)
                {
                    _snprintf_s(tmpStr,sizeof(tmpStr),sizeof(tmpStr), "%lx", dataVal);
                }
                else
                {
                    _snprintf_s(tmpStr,sizeof(tmpStr),sizeof(tmpStr), "%lu", dataVal);
                }

            break;
            case FT_UINT64:
                dataVal = tvb_get_ntoh64(tvb,offset);
                if(BASE_HEX_DEC == hfRegInfo->hfinfo.display)
                {
                    _snprintf_s(tmpStr,sizeof(tmpStr),sizeof(tmpStr), "%llx", dataVal);
                }
                else
                {
                    _snprintf_s(tmpStr,sizeof(tmpStr),sizeof(tmpStr), "%llu", dataVal);
                }
            break;
            case FT_INT8:
                dataVal = tvb_get_guint8(tvb,offset);
                _snprintf_s(tmpStr,sizeof(tmpStr),sizeof(tmpStr), "%d", dataVal);
                break;
            case FT_INT16:
                dataVal = tvb_get_ntohs(tvb,offset);
                _snprintf_s(tmpStr,sizeof(tmpStr),sizeof(tmpStr), "%d", dataVal);
                break;
            case FT_INT32:
                dataVal = tvb_get_ntohl(tvb,offset);
                _snprintf_s(tmpStr,sizeof(tmpStr),sizeof(tmpStr), "%ld", dataVal);
                break;
            case FT_INT64:
                dataVal = tvb_get_ntoh64(tvb,offset);
                _snprintf_s(tmpStr,sizeof(tmpStr),sizeof(tmpStr), "%lld", dataVal);
                break;
            case FT_STRING:
                strBuff = (char*) tvb_get_string(tvb,offset,size);
                _snprintf_s(tmpStr,sizeof(tmpStr),sizeof(tmpStr), "%s",strBuff);
                if(strBuff)
                {
                    g_free(strBuff);
                }
                break;
            case FT_BYTES:
                strBuff = (char*) tvb_get_string(tvb,offset,size);
                char buf[5];
                std::string displayValue;
                for (size_t i=0; i < size; ++i)
                {
                    _snprintf_s(buf,sizeof(buf),sizeof(buf),"%02x", strBuff[i]);
                    displayValue += buf;
                    if (i < (size - 1))
                    {
                       displayValue  += ":";
                    }
                }
                _snprintf_s(tmpStr,
                    sizeof(tmpStr),
                    sizeof(tmpStr),
                    "%s",
                    displayValue.c_str());
                if(strBuff)
                {
                    g_free(strBuff);
                }
                break;
            }
            value = tmpStr;
    }

    void
    BinaryLogWindowsHelper::createLegacyLogDisplayString(std::string& colStr,
            hf_register_info *hfRegInfo,
            std::string& logDataDisplayStr,
            tvbuff_t *tvb,
            QctUint32_t offset,
            int firstStr)
    {
        char buffer[30] ={'\0'};
        char *strBuff=NULL;
        std::string modStr;
        //Legacy binlog display assumes first string
        //is a function name, so special formatting is done
        //to ensure its displayed as the target reader here.
        if(firstStr == 0)
        {
            logDataDisplayStr += hfRegInfo->hfinfo.name;
        }

        //Legacy binlog contains only unit32/uint8/string,
        //so no other format supported in this function
        if(hfRegInfo->hfinfo.type == FT_UINT32)
        {
            QctUint32_t val = tvb_get_ntohl(tvb,offset);
            _snprintf_s(buffer,sizeof(buffer),sizeof(buffer),"=%u ",val);
            logDataDisplayStr+=buffer;
        }
        else if(hfRegInfo->hfinfo.type == FT_UINT8)
        {
            uint8_t val = tvb_get_guint8(tvb,offset);
            _snprintf_s(buffer,sizeof(buffer),sizeof(buffer),"=%u ",val);
            logDataDisplayStr += buffer;
        }
        else if(hfRegInfo->hfinfo.type == FT_STRING)
        {
            strBuff = (char*) tvb_get_string(tvb,offset,MAX_LOG_STR_LEN);
            if(firstStr == 1)
            {
                modStr = modStr + "[ " + strBuff +" ] ";
                modStr += logDataDisplayStr;
                logDataDisplayStr = modStr;
            }
            else
            {
                logDataDisplayStr += "=";
                logDataDisplayStr += strBuff;
            }
            if(strBuff)
            {
                 g_free(strBuff);
            }
        }
    }

    void
    BinaryLogWindowsHelper::createColumnInfoDisplayString(
    std::string& colStr,
    char *globalTickString,
    std::string& moduleHeaderParamsString,
    std::string& logDataDisplayStr)
    {
        colStr="";
        colStr = colStr + globalTickString + " ";
        colStr += moduleHeaderParamsString;
        colStr += " ";
        colStr += logDataDisplayStr;
    }
}



