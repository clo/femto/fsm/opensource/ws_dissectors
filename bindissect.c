/*
bindissector.c
Copyright (c) 2016, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <direct.h>
#include "gmodule.h"
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include <epan/packet.h>
#include <string.h>
#include <epan/tvbuff-int.h>
#include <stdint.h>
#include "interface.h"

static dissector_handle_t l2Dissector_handle;
static int proto_binLogDissector =-1;

#define BINLOG_DEFAULT_UDP_PORT 65534
int binlogUdpPort = BINLOG_DEFAULT_UDP_PORT;

enum ftenum ftnone = FT_NONE;
enum ftenum ftint8 = FT_INT8;
enum ftenum ftuint8 = FT_UINT8;
enum ftenum ftint16 = FT_INT16;
enum ftenum ftuint16 = FT_UINT16;
enum ftenum ftint24 = FT_INT24;
enum ftenum ftuint24 = FT_UINT24;
enum ftenum ftint32 = FT_INT32;
enum ftenum ftuint32 = FT_UINT32;
enum ftenum ftint64 = FT_INT64;
enum ftenum ftuint64 = FT_UINT64;
enum ftenum ftstr = FT_STRING;
enum ftenum ftbinstr = FT_BYTES;
enum field_display_e basenone = BASE_NONE;
enum field_display_e basehexdec = BASE_HEX_DEC;
enum field_display_e basedec = BASE_DEC;

extern hf_register_info hf[MAX_HF_NUM];
extern gint *ett[];
extern int idx;
extern int numhf;

extern gboolean WsToXmlReader();
extern int readPortFromPreference();
extern  void dissect_binLog(tvbuff_t *tvb, packet_info *pinfo, proto_tree *tree);

/****************************************************************************
 * Function Name  :proto_reg_handoff_l2Dissector
 * Inputs         :None
 * Outputs        :None
 * Returns        :None
 * Description    :This function creates the dissector and registers a routine
 *                 to be called to do the actual dissecting.
 ****************************************************************************/
void proto_reg_handoff_binlogDissector(void)
{
    static gboolean initialized=FALSE;
    if (!initialized)
    {
        l2Dissector_handle = create_dissector_handle(dissect_binLog,
            proto_binLogDissector);
        binlogUdpPort = readPortFromPreference();
        if(binlogUdpPort != 0)
        {
            dissector_add("udp.port", binlogUdpPort, l2Dissector_handle);
        }
        else
        {
            binlogUdpPort = BINLOG_DEFAULT_UDP_PORT;
            dissector_add("udp.port", binlogUdpPort, l2Dissector_handle);
        }
    }
}
/*****************************************************************************
 * Function Name  :proto_register_l2Dissector
 * Inputs         :None
 * Outputs        :None
 * Returns        :None
 * Description    :This function registers the given protocol. The L2 Bin
 *                 Logarray structure values are mapped to hf_register_info hf
 *****************************************************************************/
void proto_register_binlogDissector (void)
{
    gboolean wsIfRetVal=FALSE;
    if (proto_binLogDissector == -1)
    {
        int i, cnt = 0, b = 0, d = 0;
        for (i = idx; i < MAX_HF_NUM; i++)
        {
            hf[i].p_id              = (int *)malloc(8);
            memset((void *)hf[i].p_id,'\0',8);
            *(hf[i].p_id)           = -1;
            hf[i].hfinfo.name       = (char*) malloc (sizeof(char) \
                * MAX_LOG_DIS_STR_LEN);
            hf[i].hfinfo.abbrev     = (char*) malloc (sizeof(char) \
                * MAX_LOG_DIS_STR_LEN);
            memset(hf[i].hfinfo.abbrev,0,MAX_LOG_DIS_STR_LEN);
            hf[i].hfinfo.type       = FT_UINT32;
            hf[i].hfinfo.display    = BASE_DEC;
            hf[i].hfinfo.strings    = NULL;
            hf[i].hfinfo.bitmask    = 0x0;
            hf[i].hfinfo.blurb      = (char*) malloc (sizeof(char) \
                * MAX_LOG_DIS_STR_LEN);
            hf[i].hfinfo.id         = HFILL;
        }

        wsIfRetVal = WsToXmlReader();

        if(!wsIfRetVal)
        {
                return;
        }
        proto_binLogDissector = proto_register_protocol("lte_binLogDissector",
            "LTEBINLOG", "binlog_dissector");
        proto_register_field_array (proto_binLogDissector, hf, numhf );
        proto_register_subtree_array (ett, 2);//array_length (ett));
        register_dissector("binlog_dissector", dissect_binLog, proto_binLogDissector);
    }
}
