/*
logOsal.h

Copyright (c) 2016, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef LOG_OSAL_H
# define LOG_OSAL_H

#include "qctTypes.h"
#include "limits.h"
/*
 * Size of the char array used to construct a log message.
 * This value is based on the limit being used by syslog.
 */
#define MAX_LOG_BUFFER_SIZE 2048

#ifdef __cplusplus
extern "C"
{
#endif

    typedef struct QctPcapHeader_s {
        QctUint32_t magicNumber_;   //< magic number
        QctUint16_t versionMajor_;  //< major version number
        QctUint16_t versionMinor_;  //< minor version number
        QctInt32_t  thiszone_;      //< GMT to local correction
        QctUint32_t sigfigs_;       //< accuracy of timestamps
        QctUint32_t snaplen_;       //< max length of captured packets, in octets
        QctUint32_t network_;       //< data link type
    } QctPcapHeader_t;

    #define BINLOG_LINK_TYPE 150
    #define ETH_LINK_TYPE 1

    #define BINLOG_PACKET_TYPE 0
    #define LEGACY_PACKET_TYPE 1
    #define SYSLOG_PACKET_TYPE 2
    #define WIRESHARK_ETH_PACKET_TYPE 3

    #define MAX_LOG_PROG_LENGTH 16
    #define MAX_LEGACY_SYSLOG_RECORD_SIZE 1500
    #define MAX_BIN_LOG_MESSAGE_SIZE 1500


    /*  The Qct log level values. */
    enum QctLogLevel
    {
        QCT_LOG_LEVEL_INVALID  = 0,
        QCT_LOG_LEVEL_CRIT     = 1, /**< Critical log message */
        QCT_LOG_LEVEL_ERROR    = 2, /**< Error log message */
        QCT_LOG_LEVEL_WARNING  = 3, /**< Warning log message */
        QCT_LOG_LEVEL_NOTICE   = 4, /**< Notice log message */
        QCT_LOG_LEVEL_INFO     = 5, /**< Informational log message */
        QCT_LOG_LEVEL_DEBUG    = 6, /**< Debug log message */
        QCT_LOG_LEVEL_DEVEL    = 7, /**< Developer log message */
        QCT_LOG_LEVEL_ALWAYS   = 8, /**< Developer log message */
        QCT_LOG_LEVEL_LAST
    };

    typedef struct
    {
        QctUint32_t timestampSec;
        QctUint32_t timestampUsec;
        QctUint32_t captureLength;
        QctUint32_t storageLength;
    }PcapRecHeader;
    typedef struct
    {
        QctUint32_t packetType;
        QctUint32_t binLogId;
        QctUint16_t binLogLevel;
        QctUint16_t binLogVersion;
        QctUint32_t sequenceNumber;
    }BinLogHeader;

#ifdef __cplusplus
}
#endif

#endif /* LOG_OSAL_H */
