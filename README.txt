Table of Contents
1   Overview
1.1 General notes
1.2 Applicable documentation
1.3 Creation of Wireshark development environment on Windows platform
1.4 Compiling Wireshark and plugins

1 Overview
The following provides instructions to install and compile Qualcomm Technologies,
Inc Wireshark Binary logging dissectors.

1.1 General notes
Using Qualcomm Technologies, Inc Binary logging dissectors requires both the installation of
Wireshark binaries and the corresponding Wireshark source code tree with which
the Qualcomm Technologies, Inc dissectors are being compiled.

1.2 Applicable Documentation
* Wireshark User's Guide
* Wireshark Developer's Guide

1.3 Creation of Wireshark development environment on Windows platform

1.3.1   Download Visual Studio 2010 VS2010Express ISO

1.3.2   Mount the VS2010 iso file using a virtual CD emulator.

1.3.3   Download and install VS2010 Service Pack 1.

1.3.4   Download Wireshark 1.8.6 Package from
        https://www.wireshark.org/download/src/all-versions/wireshark-1.8.6.tar.bz2

1.3.5   Download libxml2-2.7.8.win32 from
        http://xmlsoft.org/sources/win32/

1.3.6   Download iconv.dll from
        ftp://ftp.gnupg.org/gcrypt/binary/libiconv-1.9.1.dll.zip
        Place iconv.dll at
        C:\Windows\SysWOW64

1.3.7   Download dirent.h from
        https://github.com/tronkko/dirent/tree/master/include
		Place dirent and iconv.h in VC include path in the VC installation directory.
		C:\Program Files (x86)\Microsoft Visual Studio 10.0\VC\include


1.3.8   Install Cygwin for 32-bit Windows - https://cygwin.com/install.html
        At the "Select Packages" page, select some additional packages which are
        not installed by default
        Navigate to the required Category/Package row and, if the package has a
        "Skip" item in the "New" column, click on the "Skip" item so it shows a
        version number for:
            - Archive/unzip
            - Devel/bison
            - Devel/flex
            - Devel/git
            - Interpreters/perl
            - Utils/patch
            - Web/wget
            - asciidoc
            - Interpreters/m4
			- Utils/dos2unix

1.3.9   Get the Python 2.7 installer from http://python.org/download/ and
        install Python into the default location (C:\Python27)

1.4     Compiling Wireshark and plugins

1.4.1   Create a development directory under C:\
        For example - C:\devt\

1.4.2   Extract wireshark-1.8.6 source code to C:\<your development directory>

1.4.3   Extract the plugin source files to
        C:\<your_development_directory>\wireshark-1.8.6\plugins\binlog
        on Windows Machine

1.4.4   Referring C:\<your_development_directory>\wireshark-1.8.6\doc\README.plugins, rename
        C:\<your_development_directory>\wireshark-1.8.6\plugins\Custom.m4.example    as C:\<your_development_directory>\wireshark-1.8.6\plugins\Custom.m4
        C:\<your_development_directory>\wireshark-1.8.6\plugins\Custom.make.example  as C:\<your_development_directory>\wireshark-1.8.6\plugins\Custom.make
        C:\<your_development_directory>\wireshark-1.8.6\plugins\Custom.nmake.example as C:\<your_development_directory>\wireshark-1.8.6\plugins\Custom.nmake

1.4.5   Referring C:\<your_development_directory>\wireshark-1.8.6\doc\README.plugins, replace 'foo' in
        C:\<your_development_directory>\wireshark-1.8.6\plugins\Custom.m4
        C:\<your_development_directory>\wireshark-1.8.6\plugins\Custom.make
        C:\<your_development_directory>\wireshark-1.8.6\plugins\Custom.nmake
        with binlog
		Change the WORKSPACE_PATH in Makefile.common in C:\<developmentDirectory>\wireshark-1.8.6\plugins\binlog

1.4.6   Copy "include" folder of libxml2-2.7.8.win32 to C:\<your_development_directory>\wireshark-1.8.6\plugins\binlog

1.4.7   Place libxml2.lib  at
        C:\<your_development_directory>\wireshark-1.8.6\plugins\binlog

1.4.8   Environment variable PATH is to be modified.
        Go to
        Control panel > system > advanced system settings > environment variables > system variables > path
        Add ";" at the end of the path
        Add C:\<your_development_directory>\wireshark-1.8.6\plugins\binlog after it
		Also add C:\cygwin64\bin

1.4.9  Copy the files CMakeLists,Makefile.am,Makefile.common,Makefile.nmake to
	   C:\<your_development_directory>\wireshark-1.8.6\plugins\binlog\

1.4.10  Open the all.vcproj from the folder C:\<your_development_director\wireshark-1>\wireshark-1.8.6\
		and build it.

1.4.11  binlog.dll is created at C:\<your_development_directory>\wireshark-1.8.6\plugins\binlog

1.4.12  To dissect logs with newly created binlog.dll, place the xmls
        at C:\Program Files (x86)\Wireshark\plugins\1.8.6 , and
        binlog.dll at C:\Program Files (x86)\Wireshark\plugins\1.8.6

1.4.13  Launch Wireshark. In the menu-bar, click on 'Edit'.
        Go to 'Preferences' > 'Protocols' > 'DLT_USER' > 'Edit' > 'New'.
        Add following fields:
        DLT              :  Choose DLT = 150
        Payload protocol :  binlog_dissector
        Header size      :  0
        Trailer size     :  0
        Click on Apply.

1.4.14 Open the binary log pcap.gz files in Wireshark to see the binary log
       decoded.

