/*
binarylogXmlReader.cpp
Copyright (c) 2016, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
#include "binarylogXmlReader.hpp"
#include <cstring>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <algorithm>
#include "logOsal.h"

#if (defined(_WIN32) || defined(_WIN64))
#define strtok_r strtok_s
#endif
namespace Qct
{
    BinaryLogXmlReader::BinaryLogXmlReader(
            const std::string& xmlPath)
            :isGood_(true),
            xmlPath_(xmlPath),
            currLogId_(1)
    {
        if(getBinLogFilesFromDisk()<0)
        {
            isGood_ = false;
            return;
        }
        if(readBinLogFileAndLoadData()<0)
        {
            isGood_ = false;
            return;
        }
        logLevelStrValues_[0]="";
        logLevelStrValues_[1]="CRITICAL";
        logLevelStrValues_[2]="ERROR";
        logLevelStrValues_[3]="WARN";
        logLevelStrValues_[4]="NOTICE";
        logLevelStrValues_[5]="INFO";
        logLevelStrValues_[6]="DEBUG";
        logLevelStrValues_[7]="DEVEL";
        logLevelStrValues_[8]="ALWAYS";
    }
    bool
    BinaryLogXmlReader::isGood(
                void) const
    {
            return isGood_;
    }
    int
    BinaryLogXmlReader::getBinLogFilesFromDisk(void)
    {
        DIR * binXmlFileDir;
        if(xmlPath_.empty())
        {
            fprintf(stderr,"Invalid xmlPath provided as input\n");
            return -1;
        }
        if ((binXmlFileDir = opendir (xmlPath_.c_str())) == NULL)
        {
            fprintf(stderr,"Error in opening directory %s ",xmlPath_.c_str());
            return -1;
        }
        struct dirent *binXmlFileEnt;
        while ((binXmlFileEnt = readdir (binXmlFileDir)) != NULL)
        {
            std::string binXmlFilename(binXmlFileEnt->d_name);
            size_t offset = binXmlFilename.find(".xml");
            if (offset == std::string::npos)
            {
                continue;
            }
            binLogFileList_.push_back(xmlPath_+binXmlFilename);
        }
        closedir (binXmlFileDir);
        if(binLogFileList_.empty())
        {
            fprintf(stderr,"Error no xml files found for binary logging in\
                    %s",xmlPath_.c_str());
            return -1;
        }
        return 0;
    }

    int
    BinaryLogXmlReader::readBinLogFileAndLoadData(void)
    {
        std::vector<std::string>::iterator fItr ;
        for(fItr = binLogFileList_.begin();fItr!=binLogFileList_.end();fItr++)
        {
            std::string curFile = *fItr;
            xmlDoc *binDoc = NULL;
            xmlNode *binLogRoot = NULL;
            binDoc = xmlReadFile(curFile.c_str(), NULL, 0);
            if (binDoc == NULL)
            {
                fprintf(stderr,"Error parsing file :%s",curFile.c_str());
                return -1;
            }
            binLogRoot = xmlDocGetRootElement(binDoc);
            QctUint16_t subModId=0;
            LogData lData;
            ModuleDetails mData;
            mData.mHeaderAvailable=false;
            if(parseBinLogXml(binLogRoot,subModId,mData,lData)<0)
            {
                fprintf(stderr,"Error parseBinLogXml failed for :%s",
                        curFile.c_str());
                return -1;
            }
            logDetails_[subModId]=lData;
            moduleDetails_[subModId]=mData;
            currLogId_=1;
            xmlFreeDoc(binDoc);
            xmlCleanupParser();
        }
        return 0;
    }

    int
    BinaryLogXmlReader::parseBinLogXml(
            xmlNode * binLogRoot,
            QctUint16_t& subModId,
            ModuleDetails& mData,
            LogData& lData)
    {
        xmlNode *currNode = NULL;
        for (currNode = binLogRoot; currNode; currNode = currNode->next)
        {
            if (currNode->type == XML_ELEMENT_NODE)
            {
                if (xmlStrcmp(currNode->name,
                    (const xmlChar *)loggingSubSystemTag.c_str())==0)
                {
                    if(parseLoggingSubsystemNode(currNode,
                        subModId,
                        mData.subsystemName,
                        mData.moduleName,
                        mData.binlogVersion)<0)
                    {
                        fprintf(stderr,"Error in parseLoggingSubsystem");
                        return -1;
                    }
                }
                if (xmlStrcmp(currNode->name,
                    (const xmlChar *)logIdTag.c_str())==0)
                {
                    if(parseLogIdNode(currNode,subModId,lData)<0)
                    {
                        fprintf(stderr,"Error in parseLogId");
                        return -1;
                    }
                }
                if (xmlStrcmp(currNode->name,
                    (const xmlChar *)moduleHeaderTag.c_str())==0)
                {
                    if(parseModuleHeaderNode(currNode,subModId,mData)<0)
                    {
                        fprintf(stderr,"Error in parseModuleHeaderNode");
                        return -1;
                    }
                }
            }
            if(parseBinLogXml(currNode->children,subModId,mData,lData)<0)
            {
                fprintf(stderr,"Error in parseBinLogXml");
                return -1;
            }
        }
        return 0;
    }

    int
    BinaryLogXmlReader::parseLoggingSubsystemNode(
            xmlNodePtr& subsystemNode,
            QctUint16_t& subModId,
            std::string& subsystemName,
            std::string& moduleName,
            QctUint16_t& binlogVersion)
    {
        xmlElementPtr currElem(reinterpret_cast<xmlElementPtr>(subsystemNode));

        std::string subsystemId;
        std::string moduleId;
        std::string xmlVersion;

        if (currElem->attributes)
        {
            for (xmlAttributePtr currAttr(currElem->attributes);
                    currAttr != 0;
                    currAttr = reinterpret_cast<xmlAttributePtr>(
                        currAttr->next))
            {
                xmlChar* value(xmlGetProp(subsystemNode,
                            currAttr->name));
                if (value)
                {
                    if (xmlStrcmp(currAttr->name,
                                (const xmlChar *)subsystemTag.c_str())==0)
                    {
                        subsystemName = (char*)value;
                    }
                    if (xmlStrcmp(currAttr->name,
                                (const xmlChar *)subsystemIdTag.c_str())==0)
                    {
                        subsystemId = (char*)value;
                    }
                    if (xmlStrcmp(currAttr->name,
                                (const xmlChar *)moduleTag.c_str())==0)
                    {
                        moduleName = (char*)value;
                    }
                    if (xmlStrcmp(currAttr->name,
                                (const xmlChar *)moduleidTag.c_str())==0)
                    {
                        moduleId = (char*)value;
                    }
                    if (xmlStrcmp(currAttr->name,
                                (const xmlChar *)versionTag.c_str())==0)
                    {
                        xmlVersion = (char*)value;
                    }
                    xmlFree(value);
                }
            }
        }
        if(subsystemName.empty() || subsystemId.empty() || moduleName.empty()||
                moduleId.empty() || xmlVersion.empty())
        {
            fprintf(stderr,"Mandatory params empty\n");
            return -1;
        }
        QctUint16_t sId = atoi(subsystemId.c_str());
        QctUint16_t mId = atoi(moduleId.c_str());
        binlogVersion = atoi(xmlVersion.c_str());
        subModId= sId<<8|mId;
        return 0;
    }
    int
    BinaryLogXmlReader::parseModuleHeaderNode(
            xmlNodePtr& moduleHeaderNode,
            const QctUint16_t& subModId,
            ModuleDetails& mDetails)
    {
        xmlNode *currNode;
        for (currNode = moduleHeaderNode->children;
                currNode;
                currNode = currNode->next)
        {
            if(parseModuleHeaderParamNode(currNode,mDetails)<0)
            {
                fprintf(stderr,"Error in parseModuleHeaderParamNode\n");
                return -1;
            }
        }
        QctUint32_t strSize = 0;
        std::vector<StringParam>::iterator
            strItr=mDetails.params.strParams.begin();
        for(;strItr != mDetails.params.strParams.end();strItr++)
        {
            StringParam& curStr = *strItr;
            strSize = strSize + (curStr.length *sizeof(QctInt8_t));
        }
        QctUint32_t binstrSize = 0;
        std::vector<BinaryStringParam>::iterator
            binStrItr=mDetails.params.binstrParams.begin();
        for(;binStrItr != mDetails.params.binstrParams.end();binStrItr++)
        {
            BinaryStringParam& curBinStr = *binStrItr;
            binstrSize = binstrSize + (curBinStr.length * sizeof(QctInt8_t));
        }
        mDetails.padParam=0;
        //Padding bytes is computed based on presence of the Uint64/Int64 params
        //If there is any Uint64 or Int64 params in the Module header, we will
        //compute the size all other params and align the pad size such that the
        //entire struct size is divisible by 8 bytes
        //If Uint64 or Int64 is not present, compute the size of all other
        //params and align the pad size such that the entire struct size is
        //divisible by 4 bytes
        if((mDetails.params.int64Params.size()>0) ||
                (mDetails.params.uint64Params.size()>0))
        {
            QctUint32_t intCurrSize =
                mDetails.params.uint32Params.size() * sizeof(QctUint32_t) +
                mDetails.params.int32Params.size() * sizeof(QctInt32_t)  +
                mDetails.params.uint16Params.size()  * sizeof(QctUint16_t) +
                mDetails.params.int16Params.size() * sizeof(QctInt16_t)  +
                mDetails.params.uint8Params.size()  * sizeof(QctUint8_t) +
                mDetails.params.int8Params.size() * sizeof(QctInt8_t);

            QctUint32_t totalSize = intCurrSize +
                                    strSize +
                                    binstrSize;
             mDetails.moduleHeaderLen =
                mDetails.params.int64Params.size() * sizeof(int64_t) +
                mDetails.params.uint64Params.size() * sizeof(uint64_t) +
                totalSize;

            QctUint32_t offset = totalSize % sizeof(QctUint64_t);
            if(offset != 0)
                offset = sizeof(QctUint64_t) - (totalSize %
                        sizeof(QctUint64_t));
            mDetails.padParam=offset;
        }
        else
        {
            QctUint32_t intCurrSize =
                mDetails.params.uint16Params.size()  * sizeof(QctUint16_t) +
                mDetails.params.int16Params.size() * sizeof(QctInt16_t)  +
                mDetails.params.uint8Params.size()  * sizeof(QctUint8_t) +
                mDetails.params.int8Params.size() * sizeof(QctInt8_t);

            QctUint32_t totalSize = intCurrSize +
                                    strSize +
                                    binstrSize;
            mDetails.moduleHeaderLen =
                mDetails.params.int32Params.size() * sizeof(int32_t) +
                mDetails.params.uint32Params.size() * sizeof(uint32_t) +
                totalSize;

            QctUint32_t offset = totalSize % sizeof(QctUint32_t);
            if(offset != 0)
                offset = sizeof(QctUint32_t) - (totalSize %
                        sizeof(QctUint32_t));
            mDetails.padParam=offset;
        }
        mDetails.mHeaderAvailable = true;
        return 0;
    }

    int
    BinaryLogXmlReader::parseModuleHeaderParamNode(
                    xmlNodePtr& paramNode,
                    ModuleDetails &mDetails)
    {

        xmlElementPtr  currElem(reinterpret_cast<xmlElementPtr>(paramNode));
        if (currElem->attributes)
        {
            std::string typeTag;
            std::string nameList;
            std::string strSize;
            std::string displayFormat;
            for (xmlAttributePtr currAttr(currElem->attributes);
                    currAttr != 0;
                    currAttr = reinterpret_cast<xmlAttributePtr>(
                        currAttr->next))
            {
                xmlChar* value(xmlGetProp(paramNode,
                            currAttr->name));
                if(xmlStrcmp(currAttr->name,(const xmlChar*)"type")==0)
                {
                    typeTag = (char*)value;
                }
                if(xmlStrcmp(currAttr->name,(const xmlChar*)"nameList")==0)
                {
                    nameList = (char*)value;
                }
                if(xmlStrcmp(currAttr->name,(const xmlChar*)"size")==0)
                {
                    strSize = (char*)value;
                }
                if(xmlStrcmp(currAttr->name,(const xmlChar*)"displayFormat")==0)
                {
                    displayFormat = (char*)value;
                }
                xmlFree(value);
            }
            tokenizeAndPushToList(nameList,
                    typeTag,
                    strSize,
                    displayFormat,
                    mDetails.params,
                    mDetails.paramList);
        }
        return 0;
    }

    int
    BinaryLogXmlReader::tokenizeAndPushToList(
                    const std::string& nameList,
                    const std::string& typeTag,
                    const std::string& sSize,
                    const std::string& displayFormat,
                    FlexiParams &fParams,
                    std::vector<std::string>& pList)
    {

        DisplayFormat dType = DISPLAY_INT;
        if(!displayFormat.empty())
        {
            if( (displayFormat == "hex") &&
                (typeTag == QctUint8_tag ||
                 typeTag == QctUint16_tag ||
                 typeTag == QctUint32_tag ||
                 typeTag == QctUint64_tag))
            {
                dType = DISPLAY_HEX;
            }
            if((displayFormat == "SF-SFN") && (typeTag == QctUint32_tag))
            {
                dType = DISPLAY_SFN;
            }
        }
        char *splitStr;
        char *savePtr;
        char *origIntStr = (char*)nameList.c_str();
        while((splitStr = strtok_r(origIntStr, ",", &savePtr)))
        {
            if(typeTag == QctUint8_tag)
            {
                QctUint8Param ui8p;
                ui8p.tag = splitStr;
                ui8p.dType = dType;
                fParams.uint8Params.push_back(ui8p);
            }
            else if(typeTag == QctInt8_tag)
            {
                QctInt8Param i8p;
                i8p.tag = splitStr;
                i8p.dType = dType;
                fParams.int8Params.push_back(i8p);
            }
            else if(typeTag == QctUint16_tag)
            {
                QctUint16Param ui16p;
                ui16p.tag = splitStr;
                ui16p.dType = dType;
                fParams.uint16Params.push_back(ui16p);
            }
            else if(typeTag == QctInt16_tag)
            {
                QctInt16Param i16p;
                i16p.tag = splitStr;
                i16p.dType = dType;
                fParams.int16Params.push_back(i16p);
            }
            else if(typeTag == QctUint32_tag)
            {
                QctUint32Param ui32p;
                ui32p.tag = splitStr;
                ui32p.dType = dType;
                fParams.uint32Params.push_back(ui32p);
            }
            else if(typeTag == QctInt32_tag)
            {
                QctInt32Param i32p;
                i32p.tag = splitStr;
                i32p.dType = dType;
                fParams.int32Params.push_back(i32p);
            }
            else if(typeTag == QctUint64_tag)
            {
                QctUint64Param ui64p;
                ui64p.tag = splitStr;
                ui64p.dType = dType;
                fParams.uint64Params.push_back(ui64p);
            }
            else if(typeTag == QctInt64_tag)
            {
                QctInt64Param i64p;
                i64p.tag = splitStr;
                i64p.dType = dType;
                fParams.int64Params.push_back(i64p);
            }
            else if(typeTag == string_tag)
            {
                StringParam strParam;
                strParam.tag = splitStr;
                if(!sSize.empty())
                {
                    strParam.length = atoi(sSize.c_str());
                }
                else
                {
                    strParam.length = BINLOG_MAX_LOG_STR_LEN;
                }
                fParams.strParams.push_back(strParam);
            }
            else if(typeTag == binarystring_tag)
            {
                BinaryStringParam bstrParam;
                bstrParam.tag = splitStr;
                if(!sSize.empty())
                {
                    bstrParam.length = atoi(sSize.c_str());
                    fParams.binstrParams.push_back(bstrParam);
                }
                else
                {
                     fprintf(stderr,
                             "Size cannot be empty for binary string\n");
                     return -1;
                }
            }
            pList.push_back(splitStr);
            origIntStr = NULL;
        }
        return 0;
    }

    int
    BinaryLogXmlReader::parseLogIdNode(
            xmlNodePtr& logIdNode,
            const QctUint16_t& subsystemModId,
            LogData &lData)
    {
        xmlElementPtr currElem(reinterpret_cast<xmlElementPtr>(logIdNode));

        std::string logName;
        std::string logString;
        std::string catName;

        if (currElem->attributes)
        {
            for (xmlAttributePtr currAttr(currElem->attributes);
                    currAttr != 0;
                    currAttr = reinterpret_cast<xmlAttributePtr>(
                        currAttr->next))
            {
                xmlChar* value(xmlGetProp(logIdNode,
                            currAttr->name));
                if (value)
                {
                    if (xmlStrcmp(currAttr->name,
                                (const xmlChar *)logNameTag.c_str())==0)
                    {
                        logName= (char*)value;
                    }
                    if (xmlStrcmp(currAttr->name,
                                (const xmlChar *)logStringTag.c_str())==0)
                    {
                        logString= (char*)value;
                    }
                    if (xmlStrcmp(currAttr->name,
                                (const xmlChar *)logCategoryTag.c_str())==0)
                    {
                        catName= (char*)value;
                    }
                   xmlFree(value);
                }
            }
        }
        else
        {
            fprintf(stderr,"No attributes found on LogId node\n");
            return -1;
        }
        if(logName.empty()|| logString.empty() ||catName.empty())
        {
            fprintf(stderr,"Mandatory params in the LogId node missing\n");
            return -1;
        }
        LogDetails curLogDetails;
        curLogDetails.logString = logString;
        curLogDetails.catName = catName;
        curLogDetails.logId=currLogId_;
        curLogDetails.logName = logName;

        xmlNode *currNode = logIdNode->children;
        for (currNode = logIdNode->children;
                currNode;
                currNode = currNode->next)
        {
            if(parseLogParamNode(currNode,curLogDetails)<0)
            {
                fprintf(stderr,"Error in parseLogParamNode\n");
                return -1;
            }
        }
        lData[currLogId_]=curLogDetails;
        currLogId_++;

        return 0;
    }
    int
    BinaryLogXmlReader::parseLogParamNode(
            xmlNodePtr& paramNode,
            LogDetails& lDetails)
    {
        xmlElementPtr  currElem(reinterpret_cast<xmlElementPtr>(paramNode));

        if (currElem->attributes)
        {
            std::string typeTag;
            std::string nameList;
            std::string strSize;
            std::string displayFormat;
            for (xmlAttributePtr currAttr(currElem->attributes);
                    currAttr != 0;
                    currAttr = reinterpret_cast<xmlAttributePtr>(
                        currAttr->next))
            {
                xmlChar* value(xmlGetProp(paramNode,
                            currAttr->name));
                if(xmlStrcmp(currAttr->name,(const xmlChar*)"type")==0)
                {
                    typeTag = (char*)value;
                }
                if(xmlStrcmp(currAttr->name,(const xmlChar*)"nameList")==0)
                {
                    nameList = (char*)value;
                }
                if(xmlStrcmp(currAttr->name,(const xmlChar*)"size")==0)
                {
                    strSize = (char*)value;
                }
                if(xmlStrcmp(currAttr->name,(const xmlChar*)"displayFormat")==0)
                {
                    displayFormat= (char*)value;
                }
                xmlFree(value);
            }
            tokenizeAndPushToList(nameList,
                    typeTag,
                    strSize,
                    displayFormat,
                    lDetails.params,
                    lDetails.paramList);
        }
        return 0;
    }
    void
    BinaryLogXmlReader::getParsedData(LogAllData &logallData,
                ModAllData& modAllData)
    {
        logallData = logDetails_;
        modAllData = moduleDetails_;
    }
}
